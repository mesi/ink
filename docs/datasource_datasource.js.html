<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>JSDoc: Source: datasource/datasource.js</title>

    <script src="scripts/prettify/prettify.js"> </script>
    <script src="scripts/prettify/lang-css.js"> </script>
    <!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link type="text/css" rel="stylesheet" href="styles/prettify-tomorrow.css">
    <link type="text/css" rel="stylesheet" href="styles/jsdoc-default.css">
</head>

<body>

<div id="main">

    <h1 class="page-title">Source: datasource/datasource.js</h1>

    



    
    <section>
        <article>
            <pre class="prettyprint source linenums"><code>/**
 * @file datasource.js
 * @author Andreas Hagelberg
 *
 * Implements the DataSource class
 */

'use strict';

define(['jquery', 'jquery-ui', 'events', 'octopy', 'datasource/channel', 'secop/node_base'], function($, _, Events, Octopy, Channel, NodeBase) {

    /**
     * @class DataSource
     *
     * The DataSource class represents a data source in the backend.
     * A datasource is a piece of hardware or a program that generates
     * or accepts data signals.
     */
    class DataSource extends NodeBase {
        /**
         * @param {object} conf Datasource configuration hash received from
         *                      the backend.
         */
        constructor(conf) {
            super(conf);
            this._channels = {};
            this.addChannelCallback = conf['add channel callback'];
            this.deleteDataSourceCallback = conf['delete datasource callback'];
        }


        /**
         * Updates the object with the representation sent from the backend
         * in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        update(conf) {
            this.deserialize(conf);
        }


        /**
         * Creates the object with the representation sent from the backend
         * in JSON format.
         *
         * @param {object} conf Representation of the object.
         */
        deserialize(conf) {
            if (! ('datasource id' in conf) || !conf['datasource id']) {
                throw("Missing id for datasource");
            }
            conf['id'] = conf['datasource id'];
            conf['parent'] = this;
            this.type = conf['type'];
            super.deserialize(conf);
            this.addMetadata({ 'type': 'string', 'key': 'id', 'label': 'ID', 'value': conf['id'], 'readonly': true });
            this.addMetadata({ 'type': 'string', 'key': 'status', 'label': 'Status', 'value': conf['status'], 'readonly': true });
        }


        /**
         * Add a new channel to the data source.
         *
         * @param {object} channel_conf Configuration for the channel received
         *                              from the backend.
         */
        addChannel(channel_conf) {
            let channel_id = channel_conf['channel id'];
            channel_conf['parent'] = this;
            if (! (channel_id in this._channels)) {
                // New channel - create it
                console.debug("New channel: '" + channel_id + "'");
                channel_conf['delete channel callback'] = this.deleteChannel.bind(this);
                let new_channel = new Channel(channel_conf);
                this._channels[channel_id] = new_channel;
            }
            else {
                // Existing channel - update it
                this._channels[channel_id].update(channel_conf);
            }
        }


        /**
         * Remove old channels no longer available in the backend.
         *
         * @param {array} keep_list A list of all channel id:s that are valid,
         *                          any channel not found in this list will be
         *                          removed.
         */
        trimChannels(keep_list) {
            $.each(this._channels, function(id, channel) {
                if (! keep_list.includes(id)) {
                    this.removeChannel(id);
                }
                if (channel.datasource_id != this.id) {
                    this.removeChannel(id);
                }
            }.bind(this));
        }


        /**
         * Remove a channel from the data source
         *
         * @param {string} channel_id ID of the channel to remove.
         */
        removeChannel(channel_id) {
            if (channel_id in this._channels) {
                console.debug("Removing channel from source '" + this.id + "': '" + channel_id + "'");
                this._channels[channel_id].destroy();
                delete this._channels[channel_id];
            }
        }


        /**
         * Callback for deleting a channel. It will send back a delete
         * command to the backend, but not remove it from the GUI. This will
         * happen when the new channels list is received from the backend.
         *
         * @param {object} conf Information about the channel to delete
         * @param {string} conf['channel id'] ID of the channel to delete.
         */
        deleteChannel(conf) {
            let channel_id = conf['channel id'];
            console.debug("Delete channel: " + channel_id);
            let remove_topic = Octopy.getServerConfigValue('datasource/topics/remove channels');
            let payload = [ {
                'channel id': channel_id
            } ];
            Events.publish(remove_topic, payload);
        }


        /**
         * Event handler for the context-menu
         */
        onMenuClick(e, item) {
            super.onMenuClick(e, item);
            switch(item) {
            case 'add channel':
                this.addChannelCallback({'datasource id': this.id});
                break;

            case 'delete':
                this.deleteDataSourceCallback({'datasource id': this.id});
                break;
            }
        }


        /**
         * Creates the DOM elements for the Datasource object
         *
         * @param {HTMLElement} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            super.render(container);
            if (!this.value_el) {
                this.element.addClass('datasource');
                this._nodes_el.addClass('channels');
                // Setup context menu
                let hide_add = (this.type == "iot" || this.type == 'unknown');
                this._context_menu.context_menu({
                    'items': {
                        'add channel': { 'title': 'Add channel', 'hidden': hide_add }
                    }
                });
            }
            this.renderChannelList();
        }


        /**
         * (Re-)draws the channel list.
         */
        renderChannelList() {
            $.each(this._channels, function(id, channel) {
                channel.render(this._nodes_el);
            }.bind(this));
        }


        /**
         * Helper function for render() which does the actual creation
         * of the DOM elements.
         */
        createWidget() {
            var html = `
                &lt;div class="datasource">
                    &lt;div class="head">
                        &lt;div class="icon">&lt;/div>
                        &lt;div class="name" id="name">&lt;/div>
                        &lt;div class="status">&lt;/div>
                        &lt;button class="delete-datasource" title="Delete Datasource">&lt;/button>
                    &lt;/div>
                    &lt;button class="add-channel">Add Channel&lt;/button>
                    &lt;div class="channels">&lt;/div>
                &lt;/div>`;
            var widget = $(html);
            return widget;
        }


        /**
         * Destroys the graphical representation of the data source. It will
         * first destroy all channels and then the data source itself.
         */
        destroy() {
            $.each(this.channels, function(ch_id, channel) {
                channel.destroy();
            });
            if (this.element) {
                this.element.remove();
            }
            this.element = null;
        }
    }




    /***************************
     * DefaultDataSource class *
     ***************************/
    class DefaultDataSource extends DataSource {
        render(container) {
            super.render(container);
            this.element.find('.add-channel').hide();
        }

        addChannel(channel_conf) {
            super.addChannel(channel_conf);
            this.hideIfEmpty();
        }

        removeChannel(channel_conf) {
            super.removeChannel(channel_conf);
            this.hideIfEmpty();
        }

        trimChannels(channel_list) {
            super.trimChannels(channel_list);
            this.hideIfEmpty();
        }

        hideIfEmpty() {
            if (this._channels.length > 0) {
                this.element.show();
            }
            else {
                this.element.hide();
            }
        }

        render(container) {
            super.render(container);
            this.hideIfEmpty();
            this.element.find('.delete-datasource').hide();
        }
    }


    /*************************
     * EpicsDataSource class *
     *************************/
    class EpicsDataSource extends DataSource {
        deserialize(conf) {
            super.deserialize(conf);
            this.name = 'EPICS';
        }

        render(container) {
            super.render(container);
            this.element.find('.delete-datasource').hide();
        }
    }


    /***********************
     * IOTDataSource class *
     ***********************/
    class IOTDataSource extends DataSource {
        deserialize(conf) {
            super.deserialize(conf);
            this.prefix = conf['prefix'];
        }


        render(container) {
            super.render(container);
            this.element.find('.add-channel').hide();
        }
    }


    /**************************
     * SocketDataSource class *
     **************************/
    class SocketDataSource extends DataSource {
        deserialize(conf) {
            super.deserialize(conf);
            this.host = conf['host'];
            this.port = conf['port'];
            this.delimiter = conf['delimiter'];
            this.pollinterval = conf['pollinterval'];
        }
    }



    /**************************
     * SerialDataSource class *
     **************************/
    class SerialDataSource extends DataSource {
        deserialize(conf) {
            super.deserialize(conf);
            this.port = conf['port'];
            this.baudrate = conf['baudrate'];
            this.databits = conf['databits'];
            this.stopbits = conf['stopbits'];
            this.parity = conf['parity'];
            this.delimiter = conf['delimiter'];
            this.pollinterval = conf['pollinterval'];
        }
    }


    var classes = {
        'datasource': DataSource,
        'unknown': DefaultDataSource,
        'epics': EpicsDataSource,
        'iot': IOTDataSource,
        'socket': SocketDataSource,
        'serial': SerialDataSource
    };

    return classes;
});
</code></pre>
        </article>
    </section>




</div>

<nav>
    <h2><a href="index.html">Home</a></h2><h3>Classes</h3><ul><li><a href="Accessible.html">Accessible</a></li><li><a href="Channel.html">Channel</a></li><li><a href="Creates%2520a%2520new%2520object%2520with%2520the%2520configuration%2520given%2520as%2520argument..html">Creates a new object with the configuration given as argument.</a></li><li><a href="DataInfo.html">DataInfo</a></li><li><a href="DataInfoNode.html">DataInfoNode</a></li><li><a href="DataInfoValue.html">DataInfoValue</a></li><li><a href="DataSource.html">DataSource</a></li><li><a href="DataSourceManager.html">DataSourceManager</a></li><li><a href="EditPanel.html">EditPanel</a></li><li><a href="EnumWidget.html">EnumWidget</a></li><li><a href="Events.html">Events</a></li><li><a href="IndicatorWidget.html">IndicatorWidget</a></li><li><a href="LinearGaugeWidget.html">LinearGaugeWidget</a></li><li><a href="LineChartWidget.html">LineChartWidget</a></li><li><a href="LogWindow.html">LogWindow</a></li><li><a href="Modes.html">Modes</a></li><li><a href="Module.html">Module</a></li><li><a href="NodeBase.html">NodeBase</a></li><li><a href="NodeManager.html">NodeManager</a></li><li><a href="NumberWidget.html">NumberWidget</a></li><li><a href="Octopy.html">Octopy</a></li><li><a href="RadialGaugeWidget.html">RadialGaugeWidget</a></li><li><a href="SecArray.html">SecArray</a></li><li><a href="SecBlob.html">SecBlob</a></li><li><a href="SecBool.html">SecBool</a></li><li><a href="SecDouble.html">SecDouble</a></li><li><a href="SecEnum.html">SecEnum</a></li><li><a href="SecInt.html">SecInt</a></li><li><a href="SECNode.html">SECNode</a></li><li><a href="SecScaled.html">SecScaled</a></li><li><a href="SecString.html">SecString</a></li><li><a href="SecStruct.html">SecStruct</a></li><li><a href="SecTuple.html">SecTuple</a></li><li><a href="SliderWidget.html">SliderWidget</a></li><li><a href="StringWidget.html">StringWidget</a></li><li><a href="StripChartWidget.html">StripChartWidget</a></li><li><a href="ToggleWidget.html">ToggleWidget</a></li><li><a href="Watchdog.html">Watchdog</a></li><li><a href="Widget.html">Widget</a></li><li><a href="WidgetsPalette.html">WidgetsPalette</a></li></ul>
</nav>

<br class="clear">

<footer>
    Documentation generated by <a href="https://github.com/jsdoc/jsdoc">JSDoc 3.6.7</a> on Mon Oct 25 2021 15:41:11 GMT+0200 (Central European Summer Time)
</footer>

<script> prettyPrint(); </script>
<script src="scripts/linenumber.js"> </script>
</body>
</html>
