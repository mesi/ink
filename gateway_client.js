const mqtt = require('mqtt');
const EventEmitter = require('events').EventEmitter;
const path = require('path');

class GatewayClient extends EventEmitter {

    constructor(wsClient, host, port, username, password, server_config) {
        super();
        this.ws = wsClient;
        this.mqtt = null;
        this.id = + new Date();
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.server_config = server_config;
        this.topics = [ '#' ];  // Listen to all messages under the prefix
        this._connected = false;
        console.log('Ink:DEBUG:Client ' + this.id + ' connected to WebSocket');
    }

    connect() {
        // Connect to MQTT server
        this.mqtt = mqtt.connect({host: this.host, port: this.port,  username: this.username, password: this.password });

        // Setup event handler for MQTT connection errors
        this.mqtt.on('error', (err) => {
            console.log(err);
            this.stop();
        });

        // Setup event handler for MQTT connection established
        this.mqtt.on('connect', () => {
            this._connected = true;
            //      console.log('Client ' + this.id + ' connected to MQTT server');
            // MQTT subscriptions
            this.topics.forEach((topic) => {
                this.mqtt.subscribe(topic, {qos: 0});
            });
            // Send server config to the client on
            this.publishToWebSocket('octopy-config', JSON.stringify(this.server_config));
            // Connect event handler for forwarding messages from the web client to the MQTT server
            this.ws.on('message', this.publishToMqtt.bind(this));
            // Connect event handler for forwarding messages from MQTT server to web client via websocket
            this.mqtt.on('message', this.publishToWebSocket.bind(this));
        });

        // Setup event handler for MQTT connection close
        this.mqtt.on('close', () => {
            this.stop();
            // TODO: Add reconnect and recovery if applicable
        });
    }


    // Forward messages received from web client to MQTT server
    publishToMqtt(message) {
        let msg = JSON.parse(message);
        let topic = msg.topic;
        let payload = JSON.stringify(msg.payload);
        this.mqtt.publish(topic, payload, { "qos": 1 });
    }


    // Forward messages from MQTT server to web client via websocket
    publishToWebSocket(topic, message) {
        let msg = topic + "\n" + message;
        if (this.ws && this.ws.readyState === 1) {
            this.ws.send(msg);
        }
        else {
            this.stop();
            // TODO: Add reconnect and reccovery
        }
    }


    stop() {
        if (this._connected) {
            this._connected = false;
            this.ws.close();
            console.log('Ink:DEBUG:Client ' + this.id + ' disconnected from WebSocket');
            // Signal Ink server connection
            this.mqtt.publish(path.join(this.server_config['mqtt']['prefix'], '/ink/connections', this.id + ''), 'disconnected');
            this.mqtt.end();
            console.log('Ink:DEBUG:Client ' + this.id + ' disconnected from MQTT server');
            this.emit('close');
        }
    }


    connected() {
        return this._connected;
    }
}

module.exports = GatewayClient;
