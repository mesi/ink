/**
 * @file edit_panel.js
 * @author Andreas Hagelberg
 *
 * Implements the Edit panel module
 */


'use strict';

define(['jquery', 'modes'], function($, Modes) {

    /**
     * @class EditPanel
     *
     * This module handles the edit panel in the interface in which other
     * modules can add their controls by calling the addModule() method. This
     * module adds an "Edit" button to the main toolbar and when the user clicks
     * this button the edit panel is shown. The edit button is also changed to
     * a "Save" button. When Save is clicked the edit panel is hidden again.
     */
    class EditPanel {
        /**
         * Starts the module. Creates the needed GUI elements using the render
         * method and starts listening to changes to the edit mode.
         */
        start() {
            console.debug("Starting edit panel");
            this.render();
            $(Modes).on('change', this.onModeChange.bind(this));
        }


        /**
         * Callback for mode change. Checks if the state of the edit mode changes.
         *
         * @param {event} e Event object
         * @param {string} mode The mode that changed state
         * @param {bool} state The active state of the mode - true = active, false = inactive.
         */
        onModeChange(e, mode, state) {
            if (mode === 'edit' && state) {
                this.onEditStart();
            }
            else if(mode == 'edit' && !state) {
                this.onEditEnd();
            }
        }


        /**
         * Helper method for the onModeChange callback for when the "edit" mode
         * is activated.
         */
        onEditStart() {
            this.widget.show();
        }


        /**
         * Helper method for the onModeChange callback for when the "edit" mode
         * is deactivated.
         */
        onEditEnd() {
            this.widget.hide();
        }


        /**
         * Adds the given DOM element to the edit panel.
         *
         * @param {DOM-element} element The DOM element to add to the panel.
         */
        addModule(element) {
            this.render();
            this._content_el.append(element);
        }


        /**
         * Draws/updates the graphical representation of the edit panel and adds
         * buttons to the main toolbar.
         */
        render() {
            if (this.widget) {
                return;
            }
            this.widget = $('body').find('#edit-panel');
            this._content_el = $('<div class="content"></div>');
            this.widget.append(this._content_el);
            if (! Modes.isInMode('edit')) {
                this.widget.hide();
            }
            // Add edit button to toolbar
            let toolbar = $('#main-toolbar');
            let btnEdit = $('<button>EDIT</button>');
            btnEdit.on('click', function(e) {
                Modes.toggleMode('edit');
                if (Modes.isInMode('edit')) {
                    btnEdit.text('SAVE');
                }
                else {
                    btnEdit.text('EDIT');
                }
            }.bind(this));
            toolbar.append(btnEdit);
        }
    }

    return new EditPanel();
});
