/**
 * @file enum.js
 * @author Andreas Hagelberg
 * @description Implements the EnumWidget class
 */

'use strict';

define(['jquery', 'widgets/string'], function($, StringWidget) {

    /**
     * @class EnumWidget
     * @description The Enum Widget shows and allows the user to select
     *              a value from a set of values.
     */
    class EnumWidget extends StringWidget {
        constructor(conf) {
            super(conf);
            this._members = conf['members'];
            // Create a reverse lookup version of the members hash for quick
            // conversion of value to string.
            this._members_reverse = {};
            for (let key in this._members) {
                this._members_reverse[this._members[key]] = key;
            }
        }


        startEdit(e) {
            super.startEdit(e);
            this._input_el.on('input', function() {
                this.stopEdit(false)
            }.bind(this));
        }


        /**
         * Draws/updates the graphical representation of the widget and places
         * it in the supplied element.
         *
         * @param {HTMLElement} container The DOM element that the widget
         *                                element should be placed in.
         * @return {bool} True if the element was created, false if it already
         *                exists and was just updated.
         */
        render(container) {
            super.render(container);
            this.element.addClass('enum');
            this._input_el = $('<select class="value-input">');
            for (let key in this._members) {
                let value = this._members[key];
                let opt = $('<option value="' + value + '">' + key + '</option>');
                this._input_el.append(opt);
            }
        }


        /**
         * Updates the widget with a new value and redraws it.
         *
         * @param {value} value The integer value of the enum member to display in the widget.
         */
        value(val) {
            if (typeof(val) !== 'undefined') {
                this._value = val;
            }
            if (this._value_el) {
                this._value_el.text(this._members_reverse[this._value]);
            }
            return this._value;
        }
    }

    return EnumWidget;
});
