/**
 * @file linear-gauge.js
 * @author Andreas Hagelberg
 *
 * Implements the LinearGaugeWidget class
 */

'use strict';

define(['jquery', 'widgets/widget'], function($, Widget) {

    /**
     * @class LinearGaugeWidget
     * @extends Widget
     *
     * The Linear Gauge Widget displays numeric values as a needle on a
     * linear scale.
     */
    class LinearGaugeWidget extends Widget {
        /**
         * @constructor
         * @param {object} conf Configuration hash for the widget.
         */
        constructor(conf) {
            super(conf);
            this.max = conf['max'];
            this.min = conf['min'];
            this.unit = conf['unit'];
            this.width = conf['width'] || 350;
            this.height = conf['height'] || 40;
        }


        enableEdit() {
            // This widget is not editable
            return;
        }


        /**
         * Draws/updates the graphical representation of the widget and places
         * it in the supplied element.
         *
         * @param {DOM-element} container The DOM element that the widget
         *                                element should be placed in.
         */
        render(container) {
            super.render(container);
            this.element.addClass('linear-gauge');
            // Replace the value element with a canvas
            let canvas = $('<canvas class="value">');
            this._value_el.replaceWith(canvas);
            this._value_el = canvas;
            this._accent_color = this._value_el.css('color');
            // Remove JQuery wrapper so we have the bare DOM element
            this._canvas = canvas[0];
            // Force drawing of the canvas even if no value has been received
            this.refresh();
        }


        /**
         * Called when a new value has been received and the graphical
         * presentation of the value needs to be re-drawn.
         */
        refresh() {
            if (! this._canvas) {
                return;
            }
            let w = this.width;
            let h = this.height;
            let yMargin = 10;
            let xMargin = 20;
            this._canvas.width = w;
            this._canvas.height = h;
            let ctx = this._canvas.getContext('2d');
            //this.drawBackground(ctx, xMargin, 0, w-2*xMargin, h*0.6);
            //this.drawScale(ctx, xMargin, h*0.5, w-2*xMargin, h*0.5);
            //this.drawNeedle(ctx, xMargin, 0, w-2*xMargin, h*0.6);
            //this.drawValue(ctx, 0, 0, w, h*0.45);
            //this.drawBackground(ctx, xMargin, 0, w-2*xMargin, h*0.6);
            this.drawScale(ctx, xMargin, h*0.25, w-2*xMargin, h*0.75, h*0.25);
            this.drawNeedle(ctx, xMargin, 0, w-2*xMargin, h*0.75);
            ///this.drawValue(ctx, 0, 0, w, h*0.45);
        }


        /**
         * @private
         */
        drawBackground(ctx, x, y, w, h) {
            ctx.rect(x+0.5, y+0.5, w-1, h-1);
            ctx.strokeStyle = '#888';
            ctx.stroke();
            return;
        }


        /**
         * @private
         */
        drawScale(ctx, x, y, w, h, font_size) {
            var scaleMultipliers = [1, 1.25, 1.5, 2, 2.5, 5, 10];
            // Draw scale
            ctx.strokeStyle = '#888';
            ctx.lineWidth = 1;
            var longMarks = 10;
            var shortMarks = 5;
            var longStep = (w-1) / longMarks;
            var shortStep = longStep / shortMarks;
            var shortMarkLength = Math.floor(h * 0.4);
            var longMarkLength = Math.floor(h*0.6);
            var legendY = Math.floor(h*0.6);
            var scale = (this.max - this.min) / longMarks;
            var order = Math.floor(Math.log10(scale));
            var legendStep = Math.pow(10, order);
            for (var i = 0; i < scaleMultipliers.length; i++) {
                if (legendStep * scaleMultipliers[i] >= scale) {
                    legendStep *= scaleMultipliers[i];
                    break;
                }
            }
            var scaleMin = Math.floor(this.min / legendStep) * legendStep;
            var legend = scaleMin;
            ctx.font = font_size + 'px Helv, Helvetica, Arial';
            ctx.textAlign = 'center';
            ctx.beginPath();
            for (var xl = x; xl <= w+longStep; xl += longStep) {
                ctx.moveTo(Math.floor(xl) + 0.5, y);
                ctx.lineTo(Math.floor(xl) + 0.5, y + longMarkLength);
                if (xl < w) {
                    for (var xs = xl + shortStep; xs < xl + longStep; xs += shortStep) {
                        ctx.moveTo(Math.floor(xs) + 0.5, y);
                        ctx.lineTo(Math.floor(xs) + 0.5, y + shortMarkLength);
                    }
                }
                ctx.fillStyle = '#333';
                let legend_str = legend.toLocaleString('en-UK', {minimumIntegerDigits: 1});
                ctx.fillText(legend_str, xl, y + h);
                legend += legendStep;
            }
            this.scale = (this.max - this.min) / (legend - legendStep - scaleMin);
            ctx.stroke();
        }


        /**
         * @private
         */
        drawNeedle(ctx, x, y, w, h) {
            let pos = x - 1 + Math.ceil(w * (this._value - this.min) / (this.max - this.min) * this.scale);
            ctx.strokeStyle = this._accent_color;
            ctx.lineWidth = 2;
            ctx.beginPath();
            ctx.moveTo(pos-0.5, y);
            ctx.lineTo(pos-0.5, y + h);
            ctx.stroke();
            return;
        }


        /**
         * @private
         */
        drawValue(ctx, x, y, w, h) {
            var fontSize = h - 2;
            ctx.fillStyle = '#333';
            ctx.font = fontSize + 'px Helv, Helvetica, Arial';
            ctx.textAlign = 'center';
            ctx.fillText(this._value_string, x + w/2, y + h);
        }
    }

    return LinearGaugeWidget;
});
