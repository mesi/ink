/**
 * @file toggle.js
 * @author Andreas Hagelberg
 *
 * Implements the ToggleWidget class
 */

'use strict';

define(['jquery', 'widgets/widget'], function($, Widget) {

    /**
     * @class ToggleWidget
     * @extends Widget
     *
     * The Toggle Widget displays boolean values as a toggle switch.
     * It also allows the user to change the value.
     */
    class ToggleWidget extends Widget {
        /*
         * Callback function for when the user has clicked the toggle button.
         */
        startEdit(e) {
            e.stopPropagation();
            if (this._direct_update) {
                this.value(!this._value);
            }
            else {
                this.setBusy(true);
            }
            if (this._changeCallback) {
                this._changeCallback(this, !this._value);
            }
        }


        refresh() {
            if (this._value_el) {
                if (this._value) {
                    this.element.removeClass('state-off');
                    this.element.addClass('state-on');
                }
                else {
                    this.element.addClass('state-off');
                    this.element.removeClass('state-on');
                }
            }
        }


        /**
         * Draws/updates the graphical representation of the widget and places
         * it in the supplied element.
         *
         * @param {DOM-element} container The DOM element that the widget
         *                                element should be placed in.
         */
        render(container) {
            super.render(container);
            this.element.addClass('toggle');
            let handle = $('<span class="handle">');
            this._value_el.append(handle);
        }

    }

    return ToggleWidget;
});
