/**
 * @file widget.js
 * @author Andreas Hagelberg
 *
 * Implements the Widget base class
 */

// TODO: Allow update of settings without recreating

'use strict';

define(['jquery'], function($) {

    /**
     * @class Widget
     *
     * The Widget class is the base for all graphical widgets that display values
     * and that the user interacts with to change values for the SES.
     * They are also used for editing the node structure.
     */
    class Widget {
        /**
         * @constructor
         * @param {object} conf Configuration hash for the widget.
         */
        constructor(conf) {
            this.id = conf['id'] || conf['name'];
            this._editable = !!conf['editable'];
            this._readonly = !!conf['readonly'];
            this._label = conf['label'];
            this._show = !!conf['show'];
            this._show_label = !!conf['show label'];
            this._show_delete_button = !!conf['show delete button'];
            this._value = conf['value'];;
            this._changeCallback = conf['change callback'];
            this._deleteCallback = conf['delete callback'];
            // If direct update is true the widget will change it's value immediately
            // when the user changes it. If it is false it will not change until
            // the next call to value().
            this._direct_update = !!conf['direct update'];
        }


        editable(state) {
            if (this._readonly) {
                return;
            }
            if (typeof(state) !== 'undefined') {
                this._editable = !!state;
                if (this._editable) {
                    this.enableEdit();
                }
                else {
                    this.disableEdit();
                }
            }
            return this._editable;
        }


        enableEdit() {
            this.element.off('click');
            this.element.addClass('editable');
            if (this.startEdit) {
                this.element.on('click', this.startEdit.bind(this));
            }
        }


        disableEdit() {
            if (this.element) {
                if (this.stopEdit) {
                    this.stopEdit(true);  // Make sure we're not in edit mode
                }
                this.element.removeClass('editable');
                this.element.off('click');
            }
        }


        show(state) {
            if (typeof(state) !== 'undefined') {
                this._show = !!state;
                if (this._show) {
                    this.element.show();
                }
                else {
                    this.element.hide();
                }
            }
            return this._show;;
        }


        setBusy(state) {
            if (!this.element) {
                return;
            }
            if (state) {
                this.element.addClass('busy');
            }
            else {
                this.element.removeClass('busy');
            }
        }


        setEmpty(state) {
            if (!this.element) {
                return;
            }
            if (state) {
                this.element.addClass('empty');
            }
            else {
                this.element.removeClass('empty');
            }
        }


        showLabel(state) {
            if (typeof(state) !== 'undefined') {
                this._show_label = !!state;
                if (this._show_label) {
                    this._label_el.show();
                }
                else {
                    this._label_el.hide();
                }
            }
            return this._show_label;;
        }


        showDeleteButton(state) {
            if (typeof(state) !== 'undefined') {
                this._show_delete_button = !!state;
                if (this._show_delete_button) {
                    this._delete_button.show();
                }
                else {
                    this._delete_button.hide();
                }
            }
            return this._show_delete_button;;
        }


        /**
         * Updates the widget with a new value and redraws it.
         *
         * @param {value} value The value to display in the widget.
         */
        value(value, value_string) {
            if (typeof(value) !== 'undefined') {
                this._value = value;
                if (typeof(value_string) !== 'undefined') {
                    this._value_string = value_string;
                }
                else {
                    this._value_string = value;
                }
                this.setBusy(false);
                this.refresh();
            }
            let empty = (this._value === "" || typeof(this._value) === 'undefined');
            this.setEmpty(empty);
            return this._value;
        }


        /**
         * Called when a new value has been received and the graphical
         * presentation of the value needs to be re-drawn.
         */
        refresh() {
            if (this._value_el) {
                this._value_el.text(this._value_string);
            }
        }


        /**
         * Draws/updates the graphical representation of the widget and places
         * it in the supplied element.
         *
         * @param {DOM-element} container The DOM element that the widget
         *                                element should be placed in.
         */
        render(container) {
            console.debug('Rendering ' + this.constructor.name);
            if (this.element) {
                // Widget has already been rendered.
                return;
            }
            let html = `
                <div class="widget">
                    <label class="label"></label>
                    <div class="value"></div>
                    <div class="widget-busy-indicator"><div></div><div></div><div></div></div>
                    <button class="delete-widget-button">Delete</button>
                </div>`;
            this.element = $(html);
            this.element.attr('id', this.id);
            this._label_el = this.element.find('label');
            this._label_el.text(this._label);
            this._value_el = this.element.find('.value');
            this._delete_button = this.element.find('.delete-widget-button');
            this._delete_button.on('click', this.onDeleteClick.bind(this));
            this._delete_button.hide();
            container.append(this.element);
            this.editable(this._editable);  // Update editable state
            this.value(this._value);  // Update value display
        }


        onDeleteClick(e) {
            if (this._deleteCallback) {
                this._deleteCallback(this);
            }
        }


        /**
         * Destroys the graphical representation of the widget and removes all event triggers
         */
        destroy() {
            console.debug('Destroying Widget ' + this.constructor.name);
            if (this.element) {
                this.element.off();
                this.element.remove();
                delete this.element;
            }
        }
    }


    return Widget;
});
