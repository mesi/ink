/**
 * @file number.js
 * @author Andreas Hagelberg
 *
 * Implements the NumberWidget class
 */

'use strict';

define(['jquery', 'widgets/string'], function($, StringWidget) {

    /**
     * @class NumberWidget
     * @extends StringWidget
     *
     * The Number Widget displays numeric values as plain text.
     * It also allows the user to change the value.
     */
    class NumberWidget extends StringWidget {
        constructor(conf) {
            super(conf);
            this.max = conf['max'];
            this.min = conf['min'];
            this.unit = conf['unit'];
            if ('step' in conf) {
                this.step = conf['step'];
            }
            // TODO: Use fmtstr if available to set step to max precision to display
            else if ('min' in conf && 'max' in conf) {
                this.step = (this.max - this.min) / 100;
            }
            else {
                this.step = 1;
            }
        }


        stopEdit(cancel) {
            if (! this._editing) {
                return;
            }
            this._editing = false;
            this._input_el.replaceWith(this._value_el);
            let new_value = parseFloat(this._input_el.val());
            if (!cancel && this.value() != new_value) {
                if (this._direct_update) {
                    // Direct update
                    this.value(new_value);
                }
                else {
                    // No direct update - set the widget to busy state and wait for new value from backend
                    this.setBusy(true);
                }
                // Notify parent of value change through callback if it's been set
                if (this._changeCallback) {
                    this._changeCallback(this, new_value);
                }
            }
            if (this.element) {  // Safe-guard against race condition when destroying widget directly after edit
                this.element.on('click', this.startEdit.bind(this));
            }
        }


        /**
         * Draws/updates the graphical representation of the widget and places
         * it in the supplied element.
         *
         * @param {DOM-element} container The DOM element that the widget
         *                                element should be placed in.
         */
        render(container) {
            super.render(container);
            this.element.addClass('number');
            let wrapper = $('<div class="value-wrapper"></div>');
            this.element.append(wrapper);
            wrapper.append(this._value_el);  // move value element into the wrapper
            this._input_el = $('<input type="number" class="value-input">');
            if ('min' in this && 'max' in this) {
                this._input_el.attr('min', this.min);
                this._input_el.attr('max', this.max);
            }
            this._input_el.attr('step', this.step);
        }

    }

    return NumberWidget;
});
