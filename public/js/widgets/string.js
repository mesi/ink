/**
 * @file string.js
 * @author Andreas Hagelberg
 *
 * Implements the StringWidget class
 */

'use strict';

define(['jquery', 'widgets/widget'], function($, Widget) {

    /**
     * @class StringWidget
     * @extends Widget
     *
     * The String Widget displays string values as plain text.
     * It also allows the user to change value.
     */
    class StringWidget extends Widget {
        startEdit(e) {
            e.stopPropagation();
            this._input_el.val(this._value);
            this._input_el
                .on('blur', function() {
                    this.stopEdit(false);
                }.bind(this))
                .on('keydown', function(e) {
                    if (e.keyCode == 13) {
                        this.stopEdit(false);
                    }
                    else if (e.keyCode == 27) {
                        this.stopEdit(true);
                    }
                }.bind(this));
            this.element.off('click');
            this._value_el.replaceWith(this._input_el);
            this._input_el.focus();
            this._editing = true;
        }


        stopEdit(cancel) {
            if (! this._editing) {
                return;
            }
            this._editing = false;
            this._input_el.replaceWith(this._value_el);
            let new_value = this._input_el.val();
            if (!cancel && this.value() != new_value) {
                if (this._direct_update) {
                    // Direct update
                    this.value(new_value);
                }
                else {
                    // No direct update - set the widget to busy state and wait for new value from backend
                    this.setBusy(true);
                }
                // Notify parent of value change through callback if it's been set
                if (this._changeCallback) {
                    this._changeCallback(this, new_value);
                }
            }
            if (this.element && this._editable) {  // Safe-guard against race condition when destroying widget directly after edit
                this.element.on('click', this.startEdit.bind(this));
            }
        }


        refresh() {
            if (this._value_el) {
                this._value_el.text(this._value_string);
            }
        }


        /**
         * Draws/updates the graphical representation of the widget and places
         * it in the supplied element.
         *
         * @param {DOM-element} container The DOM element that the widget
         *                                element should be placed in.
         */
        render(container) {
            super.render(container);
            this.element.addClass('string');
            let wrapper = $('<div class="value-wrapper"></div>');
            this.element.append(wrapper);
            wrapper.append(this._value_el);  // move value element into the wrapper
            this._input_el = $('<input type="text" class="value-input">');
        }

    }

    return StringWidget;
});
