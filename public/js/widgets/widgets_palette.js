/**
 * @file widgets_panel.js
 * @author Andreas Hagelberg
 *
 * Implements the Widgets Panel
 */


'use strict';

define(['jquery', 'jquery-ui', 'modes', 'edit_panel',
        'widgets/enum', 'widgets/string', 'widgets/number',
        'widgets/toggle', 'widgets/indicator', 'widgets/radial-gauge',
        'widgets/slider', 'widgets/linear-gauge', 'widgets/stripchart', 'widgets/linechart'],
       function($, _, Modes, EditPanel,
                EnumWidget, StringWidget, NumberWidget,
                ToggleWidget, IndicatorWidget, RadialGaugeWidget, SliderWidget,
                LinearGaugeWidget, StripChartWidget, LineChartWidget)
{

    /**
     * @class WidgetsPalette
     *
     * Displays all available widgets for data visualization as a toolbox in the
     * edit panel. It also provides a method - createWidget() - for creating a
     * widget object of a specific class by providing the class name as a string.
     */
    class WidgetsPalette {
        constructor() {
            this._widget_types = {
                'StringWidget': StringWidget,
                'NumberWidget': NumberWidget,
                'SliderWidget': SliderWidget,
                'EnumWidget':   EnumWidget,
                'ToggleWidget': ToggleWidget,
                'IndicatorWidget': IndicatorWidget,
                'RadialGaugeWidget': RadialGaugeWidget,
                'LinearGaugeWidget': LinearGaugeWidget,
                'StripChartWidget':  StripChartWidget,
                'LineChartWidget': LineChartWidget
            };
        }


        /**
         * Starts the module
         */
        start() {
            console.debug("Starting widgets palette");
            this.render();
        }


        /**
         * Factory method that creates a new Widget object of the appropriate
         * class based on the type-string.
         *
         * @param {string} type The class-name of the widget to create.
         * @param {object} conf Configuration parameters for the widget.
         */
        createWidget(type, conf) {
            if (! (type in this._widget_types)) {
                throw("Unsupported widget type: " + type);
            }
            let widget = new this._widget_types[type](conf);
            return widget;
        }


        /**
         * Draws/updates the graphical representation of the manager.
         */
        render(container) {
            if (this.widget) {
                return;
            }
            let html = `
                <div id="widgets-palette">
                    <div class="title">Widgets</div>
                    <div class="widget-buttons"></div>
                </div>`;
            this.widget = $(html);
            let buttons_el = this.widget.children('.widget-buttons');
            EditPanel.addModule(this.widget);
            /* Create palette buttons for all available widget types */
            Object.keys(this._widget_types).forEach(function (w_type) {
                html = `
                    <div class="widget-button">
                        <div class="icon"></div>
                        <div class="title"></div>
                    </div>`;
                let button = $(html);
                button.attr('id', w_type);
                button.data('type', w_type);
                button.find('.title').html(w_type);
                buttons_el.append(button);
                button.draggable({
                    'snap': '.datainfo',
                    'revert': 'invalid',
                    'helper': 'clone',
                    'appendTo': 'body'
                });
            }.bind(this));
        }
    }

    return new WidgetsPalette();
});
