/**
 * @file radial-gauge.js
 * @author Andreas Hagelberg
 *
 * Implements the RadialGaugeWidget class
 */

'use strict';

define(['jquery', 'widgets/widget'], function($, Widget) {

    /**
     * @class RadialGaugeWidget
     * @extends Widget
     *
     * The Radial Gauge Widget displays numeric values as a needle on a
     * half-circle scale as well as numbers.
     */
    class RadialGaugeWidget extends Widget {
        constructor(conf) {
            super(conf);
            this.max = conf['max'];
            this.min = conf['min'];
            this.unit = conf['unit'];
            this.width = conf['width'] || 120;
            this.height = conf['height'] || 60;
        }

        enableEdit() {
            // This widget is not editable
            return;
        }


        /**
         * Draws/updates the graphical representation of the widget and places
         * it in the supplied element.
         *
         * @param {DOM-element} container The DOM element that the widget
         *                                element should be placed in.
         */
        render(container) {
            super.render(container);
            this.element.addClass('radial-gauge dial');
            // Replace the value element with a canvas
            let canvas = $('<canvas class="value">');
            this._value_el.replaceWith(canvas);
            this._value_el = canvas;
            this._accent_color = this._value_el.css('color');
            // Remove JQuery wrapper so we have the bare DOM element
            this._canvas = canvas[0];
            // Force drawing of the canvas even if no value has been received
            this.refresh();
        }


        /**
         * Called when a new value has been received and the graphical
         * presentation of the value needs to be re-drawn.
         */
        refresh() {
            if (! this._canvas) {
                return;
            }
            let w = this.width;
            let h = this.height;
            let yMargin = 5;
            this._canvas.width = w;
            this._canvas.height = h;
            let ctx = this._canvas.getContext('2d');
            this.drawScale(ctx, 0, 0, w, h-yMargin, 16);
            this.drawNeedle(ctx, 0, 0, w, h-yMargin, 20);
            this.drawValue(ctx, w/2, (h-yMargin) * 0.6, (h-yMargin) * 0.3);
        }


        /**
         * @private
         */
        drawScale(ctx, x, y, w, h, thickness) {
            ctx.beginPath();
            let r = Math.min(w/2, h);
            ctx.arc(x+w/2, y+h, r, Math.PI, 0);
            r = Math.min(w/2, h) - thickness;
            ctx.arc(x+w/2, y+h, r, 0, Math.PI, true);
            ctx.closePath();
            ctx.fillStyle = 'rgba(0, 0, 0, 0.25)';
            ctx.fill();
        }


        /**
         * @private
         */
        drawValue(ctx, x, y, h) {
            ctx.font = h + 'px Helv, Helvetica, Arial';
            ctx.textAlign = 'center';
            ctx.fillStyle = '#333';
            ctx.fillText(this._value_string, x, y + h);
        }


        /**
         * @private
         */
        drawNeedle(ctx, x, y, w, h, offset) {
            let r = Math.min(w/2, h);
            let pos = (this._value - this.min) / (this.max - this.min);
            let r_inner = r - offset;
            ctx.strokeStyle = this._accent_color;
            ctx.lineWidth = 2;
            ctx.beginPath();
            ctx.moveTo(x + w/2 - Math.cos(Math.PI * pos) * r_inner, y + h - Math.sin(Math.PI * pos) * r_inner);
            ctx.lineTo(x + w/2 - Math.cos(Math.PI * pos) * r,       y + h - Math.sin(Math.PI * pos) * r);
            ctx.closePath();
            ctx.stroke();
            return;
            ctx.save();
            ctx.shadowBlur = Math.floor(r*0.05);
            ctx.shadowColor = 'black';
            ctx.shadowOffsetX = ctx.shadowBlur * 0.5;
            ctx.shadowOffsetY = 0;
            ctx.beginPath();
            ctx.moveTo(x + w/2 - Math.cos(Math.PI * (pos+0.02)) * r_inner, y + h - Math.sin(Math.PI * (pos+0.02)) * r_inner);
            ctx.lineTo(x + w/2 - Math.cos(Math.PI *  pos     )  * r,       y + h - Math.sin(Math.PI *  pos)      * r);
            ctx.lineTo(x + w/2 - Math.cos(Math.PI * (pos-0.02)) * r_inner, y + h - Math.sin(Math.PI * (pos-0.02)) * r_inner);
            ctx.closePath();
            ctx.fillStyle = 'white';
            ctx.fill();
            ctx.restore();
        }

    }

    return RadialGaugeWidget;
});
