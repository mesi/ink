/**
 * @file indicator.js
 * @author Andreas Hagelberg
 *
 * Implements the IndicatorWidget class
 */

'use strict';

define(['jquery', 'widgets/widget'], function($, Widget) {

    /**
     * @class IndicatorWidget
     * @extends Widget
     *
     * The Indicator Widget displays boolean values as a LED. It also allows the
     * user to change the value by clicking the LED graphics.
     */
    class IndicatorWidget extends Widget {
        /*
         * Callback function for when the user has clicked the toggle button.
         */
        startEdit(e) {
            e.stopPropagation();
            if (this._direct_update) {
                this.value(!this._value);
            }
            else {
                this.setBusy(true);
            }
            if (this._changeCallback) {
                this._changeCallback(this, !this._value);
            }
        }


        refresh() {
            if (this._value_el) {
                if (this._value) {
                    this.element.removeClass('state-off');
                    this.element.addClass('state-on');
                }
                else {
                    this.element.addClass('state-off');
                    this.element.removeClass('state-on');
                }
            }
            if (this.element) {
                if (this.element.hasClass('trig1')) {
                    this.element.removeClass('trig1');
                    this.element.addClass('trig2');
                } else {
                    this.element.removeClass('trig2');
                    this.element.addClass('trig1');
                }
            }
        }


        /**
         * Draws/updates the graphical representation of the widget and places
         * it in the supplied element.
         *
         * @param {HTMLElement} container The DOM element that the widget
         *                                element should be placed in.
         */
        render(container) {
            super.render(container);
            this.element.addClass('indicator');
        }
    }

    return IndicatorWidget;
});
