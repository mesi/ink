/**
 * @file slider.js
 * @author Andreas Hagelberg
 *
 * Implements the SliderWidget class
 */

'use strict';

define(['jquery', 'widgets/widget'], function($, Widget) {

    /**
     * @class SliderWidget
     * @extends Widget
     *
     * The Slider Widget displays numeric values as a slider that can be
     * changed by the user.
     */
    class SliderWidget extends Widget {
        constructor(conf) {
            super(conf);
            this.max = conf['max'];
            this.min = conf['min'];
            this.unit = conf['unit'];
            if ('step' in conf) {
                this.step = conf['step'];
            }
            // TODO: Use fmtstr if available to set step to max precision to display
            else if ('min' in conf && 'max' in conf) {
                this.step = (this.max - this.min) / 100;
            }
            else {
                this.step = 1;
            }
        }


        enableEdit() {
            if (this._editable && this._slider) {
                this.element.addClass('editable');
                this._slider.prop('disabled', false);
                this._slider.on('mousedown', function(e) {
                    this._sliding = true;
                }.bind(this));
                this._slider.on('mouseup', function(e) {
                    this._sliding = false;
                }.bind(this));
                this._slider.on('mousemove', function(e) {
                    if (! this._sliding) {
                        return;
                    }
                    this._value_el.text(this._slider.val());
                }.bind(this));
                this._slider.on('change', this.onChange.bind(this));
            }
        }


        disableEdit() {
            if (this._slider) {
                this._slider.prop('disabled', true);
                this.element.removeClass('editable');
            }
        }


        onChange(e) {
            if (this._changeCallback) {
                let value = parseFloat(this._slider.val());
                this._changeCallback(this, value);
            }
            if (this._direct_update) {
                // Direct update
                this.dirty(true);
            }
            else {
                // No direct update - set the widget to busy state and wait for new value from backend
                this.setBusy(true);
            }
        }


        refresh() {
            if (this._slider && ! this._sliding) {
                this._slider.val(this._value);
                this._value_el.text(this._value);
            }
        }


        /**
         * Draws/updates the graphical representation of the widget and places
         * it in the supplied element.
         *
         * @param {HTMLElement} container The DOM element that the widget
         *                                element should be placed in.
         */
        render(container) {
            if (this.element) {
                return;
            }
            super.render(container);
            let html = `
                <div class="value-wrapper">
                    <input type="range" class="slider"></input>
                    <div class="value"></div>
                </div>`;
            let wrapper = $(html);
            this._value_el.replaceWith(wrapper);
            this._value_el = wrapper.find('.value');;
            this._slider = wrapper.find('.slider');
            if ('min' in this && 'max' in this) {
                this._slider.attr('min', this.min);
                this._slider.attr('max', this.max);
            }
            this._slider.attr('step', this.step);
            this.editable(this._editable);  // Update editable state
            this.value(this._value);  // Update value display
        }

    }

    return SliderWidget;
});
