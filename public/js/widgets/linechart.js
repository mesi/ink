/**
 * @file linechart.js
 * @author Andreas Hagelberg
 *
 * Implements the LineChartWidget class
 */

'use strict';

define(['jquery', 'widgets/widget', 'chart'], function($, Widget) {

    /**
     * @class LineChartWidget
     * @extends Widget
     *
     * The Line Chart Widget displays numeric values as a line chart. The chart
     * autoscales. It will show up to 100 values by default, but can be
     * configured to show more or less.
     */
    class LineChartWidget extends Widget {
        constructor(conf) {
            super(conf);
            this.max = conf['max'] || 100;
            this.min = conf['min'] || 0;
            this.unit = conf['unit'];
            this.width = conf['width'] || 350;
            this.height = conf['height'] || 200;
            this._history_size = conf['history size'] || 100;
        }

        enableEdit() {
            // This widget is not editable
            return;
        }


        /**
         * Updates the widget with a new value and redraws it.
         *
         * @param {value} value The value to display in the widget.
         */
        value(value, value_string) {
            if (! this._chart) {
                return;
            }
            if (typeof(value) !== 'undefined') {
                //this._value_string = value_string;
                let data = this._chart.data.datasets[0].data;
                let labels = this._chart.data.labels;
                if (data.length >= this._history_size) {
                    data.shift();
                    labels.shift();
                }
                data.push(value);
                let tstamp = new Date();
                let tstring = (tstamp.getHours() < 10 ? '0' : '') + tstamp.getHours() + ':' +
                    (tstamp.getMinutes() < 10 ? '0' : '') + tstamp.getMinutes() + ':' +
                    (tstamp.getSeconds() < 10 ? '0' : '') + tstamp.getSeconds();
                labels.push(tstring);
                this._chart.update();
            }
        }


        /**
         * Draws/updates the graphical representation of the widget and places
         * it in the supplied element.
         *
         * @param {DOM-element} container The DOM element that the widget
         *                                element should be placed in.
         */
        render(container) {
            super.render(container);
            this.element.addClass('linechart');
            // Replace the value element with a canvas
            let canvas = $('<canvas class="value"></canvas>');
            this._value_el.replaceWith(canvas);
            this._value_el = canvas;
            this._accent_color = this._value_el.css('color');
            // Remove JQuery wrapper so we have the bare DOM element
            this._canvas = canvas[0];
            this._canvas.width = this.width;
            this._canvas.height = this.height;
            // Setup chart
            let ctx = this._canvas.getContext('2d');
            this._chart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: [],
                    datasets: [{
                        label: this.unit,
                        data: [],
                        borderColor: this._accent_color,
                        borderWidth: 1,
                        tension: 0.1,
                        pointRadius: 0
                    }]
                },
                options: {
                    plugins: {
                        legend: {
                            display: false
                        }
                    },
                    animation: {
                        duration: 0
                    }
                }
            });

            // Force drawing of the canvas even if no value has been received
            this.refresh();
        }
    }

    return LineChartWidget;
});
