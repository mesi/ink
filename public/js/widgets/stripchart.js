/**
 * @file stripchart.js
 * @author Andreas Hagelberg
 *
 * Implements the StripChartWidget class
 */

'use strict';

define(['jquery', 'widgets/widget'], function($, Widget) {

    /**
     * @class StripChartWidget
     * @extends Widget
     *
     * The Strip Chart Widget displays numeric values as a very simple
     * strip-chart.
     */
    class StripChartWidget extends Widget {
        constructor(conf) {
            super(conf);
            this.max = conf['max'] || 100;
            this.min = conf['min'] || 0;
            this.unit = conf['unit'];
            this.width = conf['width'] || 300;
            this.height = conf['height'] || 40;
            this._history_size = conf['history size'] || this.width;
            this._history = [];
        }

        enableEdit() {
            // This widget is not editable
            return;
        }


        /**
         * Updates the widget with a new value and redraws it.
         *
         * @param {value} value The value to display in the widget.
         */
        value(value, value_string) {
            if (typeof(value) !== 'undefined') {
                //this._value_string = value_string;
                let scaled = 1 - (value - this.min) / (this.max - this.min);
                if (this._history.length >= this._history_size) {
                    this._history.shift();
                }
                this._history.push(scaled);
                this.refresh();
            }
        }


        /**
         * Draws/updates the graphical representation of the widget and places
         * it in the supplied element.
         *
         * @param {DOM-element} container The DOM element that the widget
         *                                element should be placed in.
         */
        render(container) {
            super.render(container);
            this.element.addClass('stripchart');
            // Replace the value element with a canvas
            let canvas = $('<canvas class="value"></canvas>');
            this._value_el.replaceWith(canvas);
            this._value_el = canvas;
            this._accent_color = this._value_el.css('color');
            // Remove JQuery wrapper so we have the bare DOM element
            this._canvas = canvas[0];
            this._canvas.width = this.width;
            this._canvas.height = this.height;
            // Force drawing of the canvas even if no value has been received
            this.refresh();
        }


        refresh() {
            if (! this._canvas) {
                return;
            }
            if (this._drawing) {
                console.debug("Too frequent updates");
                return;
            }
            this._drawing = true;
            let w = this.width;
            let h = this.height;
            let ctx = this._canvas.getContext('2d');
            this.drawBackground(ctx, 0, 0, w, h);
            this.drawScale(ctx, 0, 0, w, h);
            this.drawGraph(ctx, 0, 0, w, h);
            this._drawing = false;
        }


        drawBackground(ctx, x, y, w, h) {
            let gradient = ctx.createLinearGradient(x, y, x, y + h);
            ctx.clearRect(x, y, w, h);
        }


        drawScale(ctx, x, y, w, h) {
            ctx.beginPath();
            ctx.moveTo(x, Math.floor(y+h/2)+0.5);
            ctx.lineTo(x+w, Math.floor(y+h/2)+0.5);
            ctx.strokeStyle = '#ccc';
            ctx.stroke();
        }


        drawGraph(ctx, x, y, w, h) {
            let px = w + x;
            let py = y + ((h-1) * this._history[this._history.length-1]);
            ctx.beginPath();
            ctx.moveTo(px, py);
            let xstep = w / (this._history_size + 1);
            for (let i = this._history.length-1; i > 0; i--) {
                px-=xstep;
                if (px < x) {
                    break;
                }
                py = y + ((h-1) * this._history[i]) + 0.5;
                ctx.lineTo(px, py);
            }
            ctx.strokeStyle = this._accent_color;
            ctx.lineWidth = 1;
            ctx.stroke();
        }

}

    return StripChartWidget;
});
