'use strict';

define(['jquery', 'modes', 'sequencer/stage'], function($, Modes, Stage)
{

    class SequenceEditor {
        constructor() {
            this.stages = [];
        }


        start() {
            console.debug("Starting sequence editor");
            this.render();
        }


        addStageClick(e) {
            let stage_id = 'Stage ' + (this.stages.length + 1);
            let stage_conf = {
                'id': stage_id,
                'type': 'expression'
            }
            let stage = new Stage(stage_conf);
            this.stages.push(stage);
            stage.render(this._body_el);
        }


        render() {
            if (this.widget) {
                return;
            }
            let html = `
                <div id="sequence-editor" class="panel">
                    <div class="head">
                        <div class="name">Sequence Editor</div>
                    </div>
                    <div class="toolbar">
                        <button id="add-stage">Add Stage</button>
                    </div>
                    <div class="body stages">
                    </div>
                </div>`;
            this.widget = $(html);
            this._body_el = this.widget.children('.stages');
            this.widget.find('.toolbar #add-stage').on('click', this.addStageClick.bind(this));
            $('#workspace').append(this.widget);
            this.widget.hide();
        }
    }

    return new SequenceEditor();
});
