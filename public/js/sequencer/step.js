'use strict';

define(['jquery', 'jquery-ui'], function($, _) {

    class Step {
        constructor(conf) {
            this.id = conf['id'];
        }


        render(container) {
            if (this.widget) {
                return;
            }
            let html = `
                <div class="sequence-step expression">
                    <div class="head"><div class="name"></div></div>
                    <div class="body"></div>
                </div>`;
            this.widget = $(html);
            this.widget.draggable();
            container.append(this.widget);
            this.input = $('<div class="input"></div>');
            this.widget.append(this.input);
            this.output = $('<div class="output"></div>');
            this.widget.append(this.output);
            //this.input.draggable({
            //    'opacity': 0.5,
            //    'helper': 'clone'
            //});
            this.output.draggable({
                'opacity': 0.5,
                'helper': 'clone'
            });
        }
    }


    class ExpressionStep extends Step {
        constructor(conf) {
            super(conf);
            this.expression = conf['expression']
        }
    }



    let classes = {
        'step': Step,
        'expression': ExpressionStep
    }


    function factory(conf) {
        let type = conf['type']
        if (! (type in classes)) {
            throw("Unsupported step type: " + type);
        }
        let step = new classes[type](conf);
        return step;
    }



    return factory;
});

