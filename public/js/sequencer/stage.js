'use strict';

define(['jquery', 'sequencer/step'], function($, StepFactory) {

    class Stage {
        constructor(conf) {
            this.steps = [];
            this.serialize(conf);
        }


        serialize(conf) {
            this.id = conf['id'];
            if (this._name_el) {
                this._name_el.html(this.id);
            }
        }


        addStepClick(e) {
            let step_id = 'Step ' + (this.steps.length + 1);
            let step_conf = {
                'id': step_id,
                'type': 'expression'
            }
            let step = StepFactory(step_conf);
            this.steps.push(step);
            step.render(this._body_el);
        }


        render(container) {
            if (this.widget) {
                return;
            }
            let html = `
                <div class="sequence-stage">
                    <div class="head">
                        <div class="name">Stage</div>
                    </div>
                    <div class="toolbar">
                        <button id="add-step">Add Step</button>
                    </div>
                    <div class="body steps"></div>
                </div>`;
            this.widget = $(html);
            this._body_el = this.widget.find('.steps');
            this._name_el = this.widget.find('.head > .name');
            console.debug(this._name_el);
            this._name_el.html(this.id);
            this.widget.find('.toolbar #add-step').on('click', this.addStepClick.bind(this));
            container.append(this.widget);
        }
    }

    return Stage;
});

