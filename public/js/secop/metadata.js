define(['widgets/widgets_palette'],
       function(WidgetsPalette)
{

    /*
     * A Mixin class that implements and handles metadata values. These values
     * are just key-value pairs but they can be treated separately from other
     * member attributes so that they can be saved and restored and displayed.
     */
    class Metadata {
        /*
         * Copies all methods in this class into another.
         */
        static include(constructor) {
            const descriptors = Object.getOwnPropertyDescriptors(Metadata.prototype);
            Object.keys(descriptors).forEach(name => {
                Object.defineProperty(constructor.prototype, name, descriptors[name]);
            });
        }


        initMetadata() {
            this._metadata = {};
        }


        /**
         * Adds a new named metadata field to the node. Metadata fields will be shown in the GUI
         * and can be edited by the user in edit mode.
         *
         * @param {object} conf Configuration for the metadata
         * @param {string} conf.key Unique id of the metadata field
         * @param {string} conf.type Type of metadata - must be a valid SECoP data type identifier
         * @param {string} conf.label This is the name of the metadata that is shown to the user
         * @param {value} conf.value The value of the metadata field
         */
        addMetadata(conf) {
            let type = conf['type'];
            let key = conf['id'] || conf['key'];
            let value = conf['value'];
            if (key in this._metadata) {
                // Metadata already exists - just update it
                this._metadata[key].value(value);
            }
            else {
                // New metadata field
                conf['id'] = key;
                conf['change callback'] = function(src, value) {
                    for (var key in this._metadata) {
                        if (src === this._metadata[key]) {
                            this._metadata_dirty = true;
                            this.onMetadataChange(key, value);
                            return;
                        }
                    }
                    console.error("Got change call from unknown widget: " + src.id);
                }.bind(this);
                conf['show'] = true;
                conf['show label'] = true;
                if (typeof(value) != 'undefined') {
                    conf['value'] = value;
                }
                conf['direct update'] = true;
                /* Create appropriate widget based on the type of metadata value */
                let widget_types = {
                    'string': 'StringWidget',
                    'number': 'NumberWidget',
                    'int': 'NumberWidget',
                    'bool': 'ToggleWidget',
                    'enum': 'EnumWidget'
                };
                let widget_type = widget_types[type];
                try {
                    this._metadata[key] = WidgetsPalette.createWidget(widget_type, conf)
                }
                catch(err) {
                    console.error("Could not create metadata widget object for key " + key + ": " + err);
                }
            }
        }


        /**
         * Changes the value of a metadata field.
         *
         * @param {string} key Identifier for the field.
         * @param {value} value The new value for this metadata.
         */
        setMetadataValue(key, value) {
            if (this._metadata && (key in this._metadata)) {
                this._metadata[key].value(value);
            }
            else {
                throw("Can't set metadata key '" + key + "' as it does not exist in the node");
            }
        }


        /**
         * Returns the current value of a metadata field.
         *
         * @param {string} key Identifier for the field.
         * @return {value} The current value for the field.
         */
        getMetadataValue(key) {
            if (this._metadata && (key in this._metadata)) {
                return this._metadata[key].value();
            }
            else {
                return undefined;
            }
        }

        /**
         * Virtual method used as callback whenever a metadata field has been
         * changed by the user.
         *
         * @param (Widget) src Widget object that the user interacted with.
         * @param (value) value The new value entered by the user.
         */
        onMetadataChange(key, value) {
            // console.error("Virtual method not overloaded: \"onMetadataChange()\"");
        }


        /**
         * Enables edit mode for all metdata fields.
         */
        enableMetadataEdit() {
            for (let key in this._metadata) {
                this._metadata[key].editable(true);
            }
        }


        /**
         * Disables edit mode for all metdata fields.
         */
        disableMetadataEdit() {
            for (let key in this._metadata) {
                this._metadata[key].editable(false);
            }
        }


        /**
         * Converts the node to a simple heirarchial structure that can be
         * converted into JSON in SECoP format.
         *
         * @return {object} SECoP compatible object ready to be converted to JSON
         */
        serializeMetadata() {
            let structure = {};
            for (let key in this._metadata) {
                let val = this._metadata[key].value();
                if (typeof(val) !== 'undefined') {
                    structure[key] = val;
                }
            }
            return structure;
        }


        renderMetadata(container) {
            for (let key in this._metadata) {
                this._metadata[key].render(container);
                this._metadata[key].element.addClass('metadata');
            }
        }


        /**
         * Destroys the graphical representation of the metadata fields
         * and deletes all metadata fields.
         */
        destroyMetadata() {
            for (let key in this._metadata) {
                this._metadata[key].destroy();
            }
            this._metadata = null;
        }
    }


    // This class is meant to be used as a mixin so we remove the constuctor
    // so the class can't be instantiated.
    delete Metadata.prototype.constructor;

    return Metadata;
});
