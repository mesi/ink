/**
 * @file node_base.js
 * @author Andreas Hagelberg
 *
 * Implements the NodeBase class
 */

'use strict';

define(['secop/metadata', 'modes'],
       function(Metadata, Modes)
{
    /**
     * @class NodeBase
     *
     * The NodeBase class is the super class for the various node classes (SECNode,
     * Module, Accessible, DataInfo) and implements various common functionality
     * such as handling of metadata fields.
     */
    class NodeBase {
        /**
         * @constructor
         * @param {object} conf Configuration hash for the node.
         */
        constructor(conf) {
            this.initMetadata();
            // The dirty flag is used to signal that changes have been made to
            // the modules of this node (ie added or removed). Changes to metadata are
            // checked and flagged separately.
            this._dirty = false;
            this.deserialize(conf);
            $(Modes).on('change', this.onModeChange.bind(this));
            console.debug("New " + this.constructor.name + ": '" + this.id + "'");
        }


        /**
         * Updates the object with the representation sent from the backend in JSON format.
         * An alias for deserialize().
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        update(conf) {
            console.debug("Updating " + this.constructor.name + ": " + this.id);
            this.deserialize(conf);
        }


        /**
         * Updates the object with the representation sent from the backend in JSON format.
         *
         * @param {string} conf.id ID of this node
         * @param {object} conf.parent Reference to this node's parent for back-tracking
         */
        deserialize(conf) {
            this.id = conf['id'];
            this.parent = conf['parent'];
            if (typeof(this.parent) === 'undefined') {
                throw("Node has no parent set");
            }
        }


        /**
         * Helper method for handling data specific to SecNodes, Modules and accessibles.
         * It is needed because of the retarded way SECoP deals with names and descriptions.
         *
         * @param {string} description A description string in SECoP format ("name\n\ndescription")
         */
        deserializeDescription(description) {
            let desc = description.split("\n\n", 2);
            this.addMetadata({ 'type': 'string', 'key': 'name', 'label': 'Name', 'value': desc[0] });
            this.addMetadata({ 'type': 'string', 'key': 'description', 'label': 'Description', 'value': desc[1] });
        }


        /**
         * Event callback for mode changes.
         *
         * @param {event} e Event object.
         * @param {string} mode The mode that was (de)activated.
         * @param {bool} state True if the mode was activated, false if it was deactivated
         */
        onModeChange(e, mode, state) {
            if (mode == 'edit') {
                this.enableEditing(state);
            }
        }


        /**
         * Event handler for the context-menu
         *
         * @param {event} e Event object.
         * @param {string} item The id of the menu option that the user clicked.
         */
        onMenuClick(e, item) {
            switch(item) {
            case 'edit':
                this.toggleEdit();
                break;
            }
        }


        /**
         * Helper method for the onModeChange callback for when the "edit" mode is activated.
         */
        onEditStart() {
            if (this.element) {
                this.element.addClass('edit');
            }
            if (this._context_menu) {
                this._context_menu.context_menu('setItems', {
                    'edit': { 'title': 'End edit' }
                });
            }
            this.enableMetadataEdit();
            this._editing = true;
        }


        /**
         * Helper method for the onModeChange callback for when the "edit" mode is deactivated.
         */
        onEditEnd() {
            this._editing = false;
            this.disableMetadataEdit();
            if (this.element) {
                this.element.removeClass('edit');
            }
            if (this._context_menu) {
                try {
                    this._context_menu.context_menu('setItems', {
                        'edit': { 'title': 'Edit' }
                    });
                }
                catch(err) {
                    //Ignore
                }
            }
        }


        /**
         * Toggles the edit mode
         */
        toggleEdit() {
            if (this._editing) {
                this.onEditEnd();
            }
            else {
                this.onEditStart();
            }
        }


        /**
         * Enable/disable the possibility to edit this node
         *
         * @param {bool} state Enable state, true = enable, false = disable.
         */
        enableEditing(state) {
            if (! this.element) {
                return;
            }
            if (! state) {
                // If the global edit mode is disabled we want to
                // stop the local edit mode for this node.
                this.onEditEnd();
            }
        }


        /**
         * Converts the node to a simple heirarchial structure that can be
         * converted into JSON in SECoP format.
         *
         * @return {object} SECoP compatible object ready to be converted to JSON
         */
        serialize() {
            return this.serializeMetadata();
        }


        /**
         * Helper method for handling data specific to SecNodes, Modules and accessibles.
         * It is needed because of the retarded way SECoP deals with names and descriptions.
         *
         * @return {object} SECoP compatible description string ("name\n\ndescription")
         */
        serializeDescription() {
            let name = this.getMetadataValue('name');
            let description = this.getMetadataValue('description');
            let desc;
            if (typeof(name) !== 'undefined' && typeof(description) !== 'undefined') {
                desc = name + "\n\n" + description;
            }
            else if (typeof(name) !== 'undefined') {
                desc = name;
            }
            else if (typeof(description) !== 'undefined') {
                desc = description;
            }
            else {
                desc = '';
            }
            return desc;
        }


        /**
         * Draws/updates the graphical representation of the node and places it
         * in the supplied element.
         *
         * @param {DOM-element} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            let id = this.id || this.name || (this.parent ? this.parent.id : 'unknown');
            if (! this.element) {
                console.debug('Rendering ' + this.constructor.name + ': ' + id);
                // Node has not been rendered before - create it
                // Create DOM elements
                var html = `
                    <div class="node">
                        <div class="head">
                            <div class="context-menu"></div>
                            <div class="node-type"></div>
                            <div class="icon"></div>
                        </div>
                        <div class="nodes"></div>
                    </div>
                `;
                this.element = $(html);
                this.element.attr('id', id);
                this._head_el = this.element.children('.head');
                this._toolbar_el = this._head_el.children('.toolbar');
                this._nodes_el = this.element.children('.nodes');
                let menu = this._head_el.find('.context-menu');
                // Setup context menu
                menu.context_menu({
                    'items': {
                        'edit': { 'title': 'Edit' },
                        'delete': { 'title': 'Delete' }
                    }
                });
                menu.on('select', this.onMenuClick.bind(this));
                this._context_menu = menu;
                // Insert element into the parent container
                container.append(this.element);
                // Add all meatadata fields to the head element
                this.renderMetadata(this._head_el);
            }
            // Update widget for current editing mode
            this.enableEditing(Modes.isInMode('edit'));
        }


        /**
         * Destroys the graphical representation of the node and removes all event triggers
         */
        destroy() {
            let id = this.id || this.name || (this.parent ? this.parent.id : 'unknown');
            console.debug('Destroying ' + this.constructor.name + ': ' + id);
            this.destroyMetadata();
            this._context_menu = null;
            if (this.element) {
                this.element.off();
                this.element.remove();
                delete this.element;
            }
        }


        /**
         * Returns true if the node should be visible in the current set of modes.
         *
         * @return {bool} True if it should be visible, false if not.
         */
        visible() {
            if (! this.visibility) {
                this.visibility = 'user';
                console.debug("Setting visbility for " + this.id + " to default: " + this.visibility);
            }
            let modes = Modes.getModes();
            return !!(modes[this.visibility]);
        }


        /**
         * Sets or returns the dirty flag i.e. the indicator that the node has been changed
         * by the user and should be saved. If a bool value is supplied as argument the
         * state is set to that value. If no argument it supplied the current state is returned
         * and no change is made.
         *
         * @param {bool} state If defined the dirty flag will be set to this value.
         * @return {bool} True if changes have been made, otherwise false.
         */
        dirty(state) {
            if (typeof(state) !== 'undefined') {
                // Set dirty state
                this._dirty = !!state;
                if (! this._dirty) {
                    // Clear metadata dirty flag
                    this._metadata_dirty = false;
                }
                return this._dirty;
            }
            // Don't set state, just return current state
            return (this._dirty || this._metadata_dirty);
        }
    }

    /* Add the Metadata mixin to the class */
    Metadata.include(NodeBase);

    return NodeBase;
});
