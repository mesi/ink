/**
 * @file module.js
 * @author Andreas Hagelberg
 *
 * Implements the Module class
 */

'use strict';

define(['jquery', 'events', 'secop/node_base', 'secop/accessible'], function($, Events, NodeBase, Accessible) {

    /**
     * @class Module
     * @extends NodeBase
     *
     * The Module class represents a SECoP Module. It inherits from the NodeBase
     * class and contains references to Accessible objects.
     */
    class Module extends NodeBase {
        /**
         * Updates the Module with the configuration
         *
         * @param {object} conf Module configuration
         */
        update(conf) {
            this.deserialize(conf);
            // The dirty flag is used to signal that changes have been made to
            // the accessibles of this module (ie added or removed). Changes to metadata are
            // checked and flagged separately by the super class.
            this._dirty = false;
        }


        /**
         * Updates the object with the representation sent from the backend in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        deserialize(conf) {
            super.deserialize(conf);
            this.deserializeDescription(conf['description']);
            this.addMetadata({ 'type': 'string', 'key': 'id', 'label': 'ID', 'value': conf['id'] });
            let iface_cls = '';
            if (('interface_classes' in conf) && Array.isArray(conf['interface_classes'])) {
                iface_cls = conf['interface_classes'].join(", ");
            }
            this.addMetadata({ 'type': 'string', 'key': 'interface_classes', 'label': 'Interface Classes', 'value': iface_cls });
            this.addMetadata({ 'type': 'string', 'key': 'visibility', 'label': 'Visibility', 'value': conf['visibility'] });
            this.addMetadata({ 'type': 'string', 'key': 'group', 'label': 'Group', 'value': conf['group'] });
            this.addMetadata({ 'type': 'string', 'key': 'meaning', 'label': 'Meaning', 'value': conf['meaning'] });
            this.addMetadata({ 'type': 'string', 'key': 'implementor', 'label': 'Implementor', 'value': conf['implementor'] });
            this.deserializeAccessibles(conf['accessibles']);
        }


        /**
         * Helper method for converting the accessibles part of the JSON representation
         * sent from the backend.
         *
         * @param {object} conf Representation of the accessibles collection converted from JSON.
         */
        deserializeAccessibles(conf) {
            if (! (conf instanceof Object)) {
                conf = {};
            }
            if (!this.accessibles) {
                this.accessibles = {};
            }
            let old_accessibles = {};
            // Remove accessibles that no longer exist
            let existing = Object.keys(conf);
            $.each(this.accessibles, (acc_id, acc_conf) => {
                if (! existing.includes(acc_id)) {
                    this.removeAccessible(acc_id);
                }
            });
            Object.assign(old_accessibles, this.accessibles);
            $.each(conf, (acc_id, acc_conf) => {
                acc_conf['parent'] = this;
                acc_conf['id'] = acc_id;
                if (acc_id in this.accessibles) {
                    // Accessible exists - update
                    this.accessibles[acc_id].update(acc_conf);
                    delete old_accessibles[acc_id];
                }
                else if (acc_id == 'status') {
                    // Module status.
                    // FIXME: Read status from this accessible and update
                    // visuals of Module accordingly
                }
                else {
                    // New accessible
                    this.accessibles[acc_id] = new Accessible(acc_conf);
                }
            });
            $.each(old_accessibles, function(i, acc) {
                acc.destroy();
            });
        }


        /**
         * Converts the node to a simple heirarchial structure that can be
         * converted into JSON SECoP format.
         *
         * @return {object} SECoP compatible representation of the Module ready to be converted to JSON
         */
        serialize() {
            let structure = super.serialize();
            structure['interface_classes'] = structure['interface_classes'].split(/\s*,\s*/);
            structure['description'] = this.serializeDescription();
            delete structure['name'];
            structure['accessibles'] = {};
            for (let acc_id in this.accessibles) {
                let accessible = this.accessibles[acc_id];
                let actual_id = accessible.getMetadataValue('id');
                let acc_struct = accessible.serialize();
                delete acc_struct['id'];
                structure['accessibles'][actual_id] = acc_struct;
            }
            return structure;
        }


        /**
         * Draws/updates the graphical representation of the node and places it
         * in the supplied element.
         *
         * @param {HTMLElement} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            super.render(container);
            this.element.addClass('module');
            this._nodes_el.addClass('accessibles');
            // Allow channels to be dropped on module to create Accessibles
            this.element.droppable({
                'accept': '.channel',
                'drop': this.onDropChannel.bind(this)
            });
            // Setup context menu
            this._context_menu.context_menu('setItems', {
                'add accessible': { 'title': 'Add Accessible' }
            });
            // Render the module's accessibles
            for (let acc_id in this.accessibles) {
                this.accessibles[acc_id].render(this._nodes_el);
            }
        }


        onDropChannel(e, ui) {
            // Reset channel element position - we don't want to keep the element
            // here, just get a signal that we should make an Accessible from it.
            ui.draggable.css({'left': 0, 'top': 0});
            let channel = ui.draggable.data('channel');
            console.debug("Create a new accessible from channel '" + channel.datasource_id + "/" + channel.id + "'");
            // Convert Channel to Accessible
            let ch_conf = channel.serialize();
            let acc_id = ch_conf['channel id'];
            while (acc_id in this.accessibles) {
                acc_id += '_';
            }
            let acc_conf = {
                'parent': this,
                'id': acc_id,
                'description': ch_conf['description'] || acc_id,
                'readonly': ch_conf['readonly'],
                'datainfo': ch_conf['datainfo'],
                'octopy': {
                    'links': {
                        'value topic': ch_conf['value topic'],
                        'get topic': ch_conf['get topic'],
                        'set topic': ch_conf['set topic']
                    }
                }
            };
            this.accessibles[acc_id] = new Accessible(acc_conf);
            this.dirty(true);
            this.render();
        }


        /**
         * Destroys the graphical representation of the node and sub-nodes and
         * removes all event triggers.
         */
        destroy() {
            // Destroy all accessibles
            for (let acc_id in this.accessibles) {
                this.accessibles[acc_id].destroy();
                delete this.accessibles[acc_id];
            }
            // The super method Will remove the widget from the GUI
            super.destroy();
        }


        onMenuClick(e, item) {
            super.onMenuClick(e, item);
            switch(item) {
            case 'add accessible':
                this.addEmptyAccessible();
                break;

            case 'delete':
                this.parent.removeModule(this.id);
                break;
            }
        }


        addEmptyAccessible() {
            let acc_id = 'NewAccessible-' + Math.floor(Date.now() / 1000);
            let acc_conf = {
                'id': acc_id,
                'description': acc_id,
                'parent': this,
                'readonly': false,
                'datainfo': {
                    'type': 'double'
                },
                'octopy': {
                    'links': {}
                }
            }
            console.debug("New empty accessible: " + acc_id);
            let new_accessible = new Accessible(acc_conf);
            this.addAccessible(new_accessible);
        }


        addAccessible(accessible) {
            accessible.dirty(true);
            this.accessibles[accessible.id] = accessible;
            this.dirty(true);
            accessible.render(this._nodes_el);
            // Set the new accesible in edit mode
            accessible.onEditStart();
        }


        /**
         * Removes a specific accessible from the Module, destroys the graphical
         * representation of it and sets the dirty flag for this node to indicate
         * that it has been edited and should be saved.
         *
         * @param {string} acc_id ID of the accessible to remove
         */
        removeAccessible(acc_id) {
            let accessible = this.accessibles[acc_id];
            accessible.destroy();
            delete this.accessibles[acc_id];
            this.dirty(true);
        }


        /**
         * Removes a specific accessible from the Module, destroys the graphical
         * representation of it and sets the dirty flag for this node to indicate
         * that it has been edited and should be saved.
         *
         * @param {string} acc_id ID of the accessible to duplicate
         */
        duplicateAccessible(acc_id) {
            let accessible = this.accessibles[acc_id];
            let acc_conf = accessible.serialize();
            acc_conf['parent'] = this;
            acc_conf['id'] = acc_conf['id'] + '_copy';
            let new_accessible = new Accessible(acc_conf);
            new_accessible.setMetadataValue('name', new_accessible.getMetadataValue('name') + ' (copy)');
            this.addAccessible(new_accessible);
        }


        /**
         * Sets or returns the dirty flag i.e. the indicator that the node has been changed
         * by the user and should be saved. If a bool value is aupplied as argument the
         * state is set to that value. If no argument it supplied the current state is returned
         * and no change is made.
         *
         * @param {bool} state If defined the dirty flag will be set to this value.
         * @return {bool} True if changes have been made, otherwise false.
         */
        dirty(state) {
            if (typeof(state) !== 'undefined') {
                // Set dirty state
                super.dirty(state);
                if (! this._dirty) {
                    // Clear dirty flag on all accessibles
                    for (let acc_id in this.accessibles) {
                        this.accessibles[acc_id].dirty(false);
                    }
                }
                return this._dirty;
            }
            // Don't set state, just return current state
            if (super.dirty()) {
                return true;
            }
            for (let acc_id in this.accessibles) {
                if (this.accessibles[acc_id].dirty()) {
                    return true;
                }
            }
            return false;
        }
    }

    return Module;
});
