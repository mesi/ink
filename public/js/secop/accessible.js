/**
 * @file accessible.js
 * @author Andreas Hagelberg
 *
 * Implements the Accessible class
 */

'use strict';

define(['jquery', 'events', 'octopy', 'secop/node_base', 'secop/datainfo', 'json2'], function($, Events, Octopy, NodeBase, DataInfoFactory) {

    /**
     * @class Accessible
     * @extends NodeBase
     *
     * The Accessible class represents a SECoP Accessible. It inherits from the NodeBase
     * class and contains references to Datainfo and Octopyinfo objects.
     */
    class Accessible extends NodeBase {
        /**
         * @constructor
         * @param {object} conf Configuration hash for the Accessible object.
         */
        constructor(conf) {
            super(conf);
            /*
            // Variables used for heartbeat detection
            this.last_update = null;
            this.updates_history_size = 10;
            this.updates = [];
            this.heartbeat_timeout_multiplier = 3;
            this.heartbeat_check_interval = 5000;
            this.max_heartbeat_interval = 10000;
            this.disconnected = true;
            this.check_heartbeat();
            */
            this._heartbeat_timeout = 20000;
            this._heartbeat_check_interval = 5000;
            this.connected = false;
        }


        /**
         * Updates the object with the representation sent from the backend in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        deserialize(conf) {
            super.deserialize(conf);
            this.deserializeDescription(conf['description']);
            this.addMetadata({ 'type': 'string', 'key': 'id', 'label': 'ID', 'value': conf['id'] });
            this.addMetadata({ 'type': 'bool', 'key': 'readonly', 'label': 'Read-only', 'value': (('readonly' in conf) ? conf['readonly'] : true)});
            this.addMetadata({ 'type': 'string', 'key': 'group', 'label': 'Group', 'value': conf['group'] });
            this.addMetadata({ 'type': 'string', 'key': 'constant', 'label': 'Constant', 'value': conf['constant'] });
            this.unlink();
            if (('octopy' in conf) && conf['octopy']) {
                this.deserializeOctopyInfo(conf['octopy']);
            }
            else {
                this.deserializeOctopyInfo({});
            }
            if (! ('datainfo' in conf) || typeof(conf['datainfo']) != 'object') {
                conf['datainfo'] = {
                    'type' : 'double'
                };
            }
            conf['datainfo']['widgets'] = this._widgets;
            this.deserializeDataInfo(conf['datainfo']);
            this.link();
        }


        /**
         * Helper method for converting the octopy specific part of the JSON representation
         * sent from the backend.
         *
         * @param {object} conf Representation of octopy data converted from JSON.
         */
        deserializeOctopyInfo(octopyinfo) {
            // Deserialize Links (MQTT Topics)
            let links = octopyinfo['links'];
            if (typeof(links) !== 'object' && links !== null) {
                links = {};
            }
            this.addMetadata({ 'type': 'string', 'key': 'value_topic', 'label': 'Value Topic', 'value': links['value topic'] });
            this.addMetadata({ 'type': 'string', 'key': 'set_topic', 'label': 'Set Topic', 'value': links['set topic'] });
            this.addMetadata({ 'type': 'string', 'key': 'get_topic', 'label': 'Get Topic', 'value': links['get topic'] });
            // Deserialize hints
            if ('hints' in octopyinfo) {
                let hints = octopyinfo['hints'];
                if ('ui' in hints) {
                    let ui_hints = hints['ui'];
                    this._widgets = ui_hints['widgets'] || ui_hints['controls'] || ui_hints['display'];
                }
            }
        }


        /**
         * Helper method for converting the datainfo part of the JSON representation
         * sent from the backend.
         *
         * @param {object} conf Representation of the datainfo converted from JSON.
         */
        deserializeDataInfo(conf) {
            conf['readonly'] = this.getMetadataValue('readonly');
            conf['widgets'] = this._widgets;
            conf['parent'] = this;
            if (this.datainfo) {
                // Datainfo node already exists - we can't guarantee it's of the correct
                // class so we destroy it and create a new one.
                this.datainfo.destroy();
            }
            this.datainfo = DataInfoFactory(conf);
        }


        /**
         * Converts the node to a simple heirarchial structure that can be
         * converted into JSON SECoP format.
         *
         * @return {object} SECoP compatible representation of the Accessible ready to be converted to JSON
         */
        serialize() {
            let structure = super.serialize();
            structure['datainfo'] = this.datainfo.serialize();
            let widgets = structure['datainfo']['widgets'];
            delete structure['datainfo']['widgets'];
            structure['description'] = this.serializeDescription();
            delete structure['name'];
            structure['octopy'] = {
                'links': {},
                'hints': {
                    'ui': {
                        'widgets': widgets
                    }
                }
            };
            if (this.getMetadataValue('value_topic')) {
                structure['octopy']['links']['value topic'] = this.getMetadataValue('value_topic');
                delete structure['value_topic'];
            }
            if (this.getMetadataValue('set_topic')) {
                structure['octopy']['links']['set topic'] = this.getMetadataValue('set_topic');
                delete structure['set_topic'];
            }
            if (this.getMetadataValue('get_topic')) {
                structure['octopy']['links']['get topic'] = this.getMetadataValue('get_topic');
                delete structure['get_topic'];
            }
            return structure;
        }


        /**
         * Links the accessible to the data-flow by subscribing to the relevant topics.
         * The data is received by the onMessage() callback function.
         */
        link() {
            let value_topic = this.getMetadataValue('value_topic');
            if (value_topic) {
                Events.subscribe(value_topic, this);
            }
            // Request new data in case the value_topic is slow or not sent out automatically.
            Octopy.requestDataUpdate();
            // If the accessible has a get_topic make a call to it to get new value
            if (this.getMetadataValue('get_topic')) {
                let get_topic = this.getMetadataValue('get_topic');
                Events.publish(get_topic, null);
            }
        }


        /**
         * Uninks the accessible to the data-flow.
         */
        unlink() {
            let value_topic = this.getMetadataValue('value_topic');
            if (value_topic) {
                Events.unsubscribe(value_topic, this);
            }
        }


        onSubnodeChange(node) {
            // Refresh datainfo structure to update changes of data types
            let structure = this.datainfo.serialize();
            this._widgets = structure['widgets'];
            this.deserializeDataInfo(structure);
            this.datainfo.render(this._nodes_el);
            this.dirty(true);
        }


        /**
         * Event message callback function. This function is called by the Events
         * module when a new message matching the subscription is received from the
         * backend (from the MQTT server).
         *
         * @param {object} msg The event message object containing topic and payload.
         */
        onMessage(msg) {
            if (! msg.payload) {
                console.info("Empty data message for topic: " + msg.topic);
                return false;
            }
            try {
                var data = JSON.parse(msg.payload);
            }
            catch(e) {
                console.error("Error parsing JSON message for topic: " + msg.topic);
                console.error(e);
                console.error(msg.payload);
                return false;
            }

            switch (msg.topic) {
            case this.getMetadataValue('value_topic'):
                try {
	            this.datainfo.updateFromSource(data);
                    this.updateHeartbeat();
                }
                catch(err) {
                    console.error("Could not update value from source for accessible " +
                                  this.getMetadataValue('id') + ": " + err);
                }
                break;
            case this.getMetadataValue('status_topic'):
	        //this.updateStatus(data[0], data[1]);
                break;

            case this.getMetadataValue('error_topic'):
	        //this.updateError(data);
                break;
            }
        }


        updateHeartbeat() {
            this._last_pulse = Date.now();
            if (! this.connected) {
                this.checkHeartbeat();
            }
        }


        checkHeartbeat() {
            if (this._heartbeat_timer) {
                clearTimeout(this._heartbeat_timer);
            }
            if (! this._last_pulse || (this._last_pulse + this._heartbeat_timeout < Date.now())) {
                if (this.connected) {
                    this.connected = false;
                    if (this.element) {
                        this.element.removeClass('connected');
                    }
                }
            }
            else {
                if (! this.connected) {
                    this.connected = true;
                    if (this.element) {
                        this.element.addClass('connected');
                    }
                }
            }
            this._heartbeat_timer = setTimeout(this.checkHeartbeat.bind(this), this._heartbeat_check_interval);
        }


        /**
         * This callback function is being called from the DataInfo child-node for
         * when the user has made a value change that needs to be sent to the backend
         * for updating the data source.
         *
         * @param {object} src The DataInfo child object that made the call.
         *                     There should only ever be a single one and this node already
         *                     has a reference to it, but this makes the method signature
         *                     identical for all nodes and sub-nodes and it also allows
         *                     for future expansions.
         * @param {value} val The value to be sent to the data source. The data type depends
         *                    on the datainfo type.
         */
        onValueChange(src, val) {
            let set_topic = this.getMetadataValue('set_topic');
            if (set_topic) {
                console.debug("Writing new value back to " + set_topic + ": " + val);
                Events.publish(set_topic, [val]);
            }
            else {
                console.error("Can't write to accessible " + this.id + " since it has no set topic");
            }
        }


        /**
         * Callback method that is called whenever a metadata field has been
         * changed by the user.
         *
         * @param (string) key The id of the value that was changed
         * @param (value) value The new value entered by the user.
         */
        onMetadataChange(key, value) {
            super.onMetadataChange(key, value);
            console.debug("Accessible metadata change: " + key + "=" + value);
            switch(key) {
            case 'readonly':
                this.onSubnodeChange(this);
                break;
            case 'value_topic':
                this.link();
                break;
            }
        }


        /**
         * Draws/updates the graphical representation of the node and places it
         * in the supplied element.
         *
         * @param {HTMLElement} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            super.render(container);
            this.element.addClass('accessible');
            this._nodes_el.addClass('infonodes');
            // Setup context menu
            this._context_menu.context_menu('setItems', {
                'duplicate': { 'title': 'Duplicate' }
            });
            /*
            if (this._head_el.find('.heartbeat-indicator').length === 0) {
                this._heartbeat_el = $('<div class="heartbeat-indicator"></div>');
                this._head_el.prepend(this._heartbeat_el);
            }
            */
            // Render the datainfo part
            this.datainfo.render(this._nodes_el);
            // Start checking heartbeat. No point doing this before the node has
            // been rendered and the heartbeat can be shown.
            this.checkHeartbeat();
        }


        onMenuClick(e, item) {
            super.onMenuClick(e, item);
            switch(item) {
            case 'duplicate':
                this.parent.duplicateAccessible(this.id);
                break;

            case 'delete':
                this.parent.removeAccessible(this.id);
                break;
            }
        }


        /**
         * Destroys the graphical representation of the node and sub-nodes and
         * removes all event triggers.
         */
        destroy() {
            this.unlink();
            this.datainfo.destroy();
            // The super method Will remove the widget from the GUI
            super.destroy();
        }


        /**
         * Sets or returns the dirty flag i.e. the indicator that the node has been changed
         * by the user and should be saved. If a bool value is aupplied as argument the
         * state is set to that value. If no argument it supplied the current state is returned
         * and no change is made.
         *
         * @param {bool} state If defined the dirty flag will be set to this value.
         * @return {bool} True if changes have been made, otherwise false.
         */
        dirty(state) {
            return (super.dirty(state) || this.datainfo.dirty(state));
        }
    }

    return Accessible;
});
