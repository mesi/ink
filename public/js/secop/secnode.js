/**
 * @file secnode.js
 * @author Andreas Hagelberg
 *
 * Implements the SECNode class
 */

'use strict';

define(['jquery', 'events', 'modes', 'secop/node_base', 'secop/module'], function($, Events, Modes, NodeBase, Module) {

    /**
     * @class SECNode
     * @extends NodeBase
     *
     * The SECNode class represents a SECoP Device. It inherits from the NodeBase
     * class and contains references to Module objects.
     */
    class SECNode extends NodeBase {
        /**
         * @constructor
         * @param {object} conf Configuration hash for the SecNode object.
         */
        constructor(conf) {
            super(conf);
            /*
            Events.subscribe(this.prefix + '/log/error', this.onError.bind(this));
            Events.subscribe(this.prefix + '/log/warning', this.onWarning.bind(this));
            Events.subscribe(this.prefix + '/log/info', this.onInfo.bind(this));
            Events.subscribe(this.prefix + '/log/debug', this.onDebug.bind(this));
            */
        }


        /**
         * Updates the object with the representation sent from the backend in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        deserialize(conf) {
            super.deserialize(conf);
            this.deserializeDescription(conf['description']);
            this.addMetadata({ 'type': 'string', 'key': 'equipment_id', 'label': 'Equipment ID', 'value': conf['equipment_id'] });
            this.addMetadata({ 'type': 'string', 'key': 'firmware', 'label': 'Firmware', 'value': conf['firmware'] });
            this.addMetadata({ 'type': 'string', 'key': 'implementor', 'label': 'Implementor', 'value': conf['implementor'] });
            this.addMetadata({ 'type': 'string', 'key': 'timeout', 'label': 'Timeout', 'value': conf['timeout'] });
            this.deserializeModules(conf['modules'])
        }


        /**
         * Helper method for converting the modules part of the JSON representation
         * sent from the backend.
         *
         * @param {object} conf Representation of the modules collection converted from JSON.
         */
        deserializeModules(conf) {
            if (! conf instanceof Object) {
                conf = {};
            }
            if (!this.modules) {
                this.modules = {};
            }
            var old_modules = {};
            Object.assign(old_modules, this.modules);  // Copy hash so we can later delete items from it
            $.each(conf, (mod_id, mod_conf) => {
                mod_conf['parent'] = this;
                mod_conf['id'] = mod_id;
                if (mod_id in this.modules) {
                    // Module exists - update
                    this.modules[mod_id].update(mod_conf);
                    delete old_modules[mod_id];
                }
                else {
                    // New module
                    this.modules[mod_id] = new Module(mod_conf);
                }
            });
            $.each(old_modules, (mod_id, mod) => {
                console.debug("Removing module: " + mod_id);
                mod.destroy();
                delete this.modules[mod_id];
            });
        }


        /**
         * Converts the node to a simple heirarchial structure that can be
         * converted into JSON in SECoP format.
         *
         * @return {object} SECoP compatible representation of the SECNode ready to be converted to JSON
         */
        serialize() {
            let structure = super.serialize();
            structure['description'] = this.serializeDescription();
            delete structure['name'];
            structure['modules'] = {}
            for (let mod_id in this.modules) {
                let module = this.modules[mod_id];
                let actual_id = module.getMetadataValue('id');
                let mod_struct = module.serialize();
                delete mod_struct['id'];
                structure['modules'][actual_id] = mod_struct;
            }
            return structure;
        }


        /**
         * Draws/updates the graphical representation of the node and places it
         * in the supplied element.
         *
         * @param {HTMLElement} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            super.render(container);
            this.element.addClass('secnode');
            this._nodes_el.addClass('modules');
            // FIXME: The following line should be handled by the CSS instead
            this.element.addClass('panel');
            // Render modules
            for (let mod_id in this.modules) {
                this.modules[mod_id].render(this._nodes_el);
            }
            // Remove the delete option from the context menu
            this._context_menu.context_menu('setItems', {
                'add module': { 'title': 'Add Module' },
                'delete': { 'hidden': true }
            });
        }


        onMenuClick(e, item) {
            super.onMenuClick(e, item);
            switch(item) {
            case 'add module':
                this.addModule();
                break;
            }
        }


        /**
         * Destroys the graphical representation of the node and sub-nodes and
         * removes all event triggers.
         */
        destroy() {
            // Destroy all modules
            for (let mod_id in this.modules) {
                this.modules[mod_id].destroy();
                delete this.modules[mod_id];
            }
            // The super method Will remove the widget from the GUI
            super.destroy();
        }


        /**
         * Event callback for clicks on the "Add Module" button. The callback link
         * is setup in the render() method. It will add a new empty Module to the SECNode.
         *
         * @param {event} e Event object.
         */
        addModule(e) {
            let mod_id = 'NewModule-' + Math.floor(Date.now() / 1000);
            let mod_conf = {
                'id': mod_id,
                'description': mod_id,
                'parent': this,
                'interface_classes': '',
                'visibility': 'user'
            }
            console.debug("New blank module: " + mod_id);
            let new_module = new Module(mod_conf);
            new_module.dirty(true);
            this.modules[mod_id] = new_module;
            this.dirty(true);
            new_module.render(this._nodes_el);
            // Set the new module in edit mode
            new_module.onEditStart();
        }


        /**
         * Removes a specific module from the SECNode, destroys the graphical
         * representation of it and sets the dirty flag for this node to indicate
         * that it has been edited and should be saved.
         */
        removeModule(mod_id) {
            let module = this.modules[mod_id];
            module.destroy();
            delete this.modules[mod_id];
            this.dirty(true);
        }


        /**
         * Sets or returns the dirty flag i.e. the indicator that the node has been changed
         * by the user and should be saved. If a bool value is aupplied as argument the
         * state is set to that value. If no argument it supplied the current state is returned
         * and no change is made.
         *
         * @param {bool} state If defined the dirty flag will be set to this value.
         */
        dirty(state) {
            if (typeof(state) !== 'undefined') {
                // Set dirty state
                super.dirty(state);
                if (! this._dirty) {
                    // Clear dirty flag on all modules
                    for (let mod_id in this.modules) {
                        this.modules[mod_id].dirty(false);
                    }
                }
                return this._dirty;
            }
            // Don't set state, just return current state
            if (super.dirty()) {
                return true;
            }
            for (let mod_id in this.modules) {
                if (this.modules[mod_id].dirty()) {
                    return true;
                }
            }
            return false;
        }

/*
        onError(msg) {
            msg = msg.message;
            msg.level = 'error';
            this.element.apparatus('log_message', msg);
        }


        onWarning(msg) {
            msg = msg.message;
            msg.level = 'warning';
            this.element.apparatus('log_message', msg);
        }


        onInfo(msg) {
            msg = msg.message;
            msg.level = 'info';
            this.element.apparatus('log_message', msg);
        }


        onDebug(msg) {
            msg = msg.message;
            msg.level = 'debug';
            this.element.apparatus('log_message', msg);
        }
*/

    }


    return SECNode;
});
