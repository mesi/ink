/**
 * @file datainfo.js
 * @author Andreas Hagelberg
 *
 * Implements the DataInfo class and sub-classes.
 * The Datainfo class and all sub-classes represents the datainfo part of a
 * SECoP Accessible. They inherit from the NodeBase class and uses various
 * GUI widgets to display the values to and receive changes from the user.
 * When included through Require.js this module returns a reference to a
 * factory function that is used to create the DataInfo objects. The factory
 * function will check the 'type' field of the SECoP-compatible datainfo
 * configuration and create an object of the correct type base don that.
 */

'use strict';

define(['jquery', 'jquery-ui', 'events', 'json2', 'secop/node_base',
        'widgets/widgets_palette'],
       function($, _, Events, __, NodeBase, WidgetsPalette)
{

    /**
     * @class DataInfo
     * @extends NodeBase
     *
     * Base-class for the DataInfoNode DataInfoValue classes.
     * Implements shared functionality that is unique to all DataInfo classes,
     * but not specific to the DataInfoNode and DataInfoValue subclasses -
     * mainly shared metadata values.
     */
    class DataInfo extends NodeBase {
        /**
         * Updates the object with the representation sent from the backend in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        deserialize(conf) {
            super.deserialize(conf);
            this.addMetadata({
                'type': 'enum',
                'key': 'type',
                'label': 'Type',
                'value': conf['type'],
                'members': {
                    'Scaled': 'scaled',
                    'Integer': 'int',
                    'Double': 'double',
                    'Boolean': 'bool',
                    'Enum': 'enum',
                    'String': 'string',
                    'Binary blob': 'blob',
                    'Array': 'array',
                    'Tuple': 'tuple',
                    'Struct': 'struct'
                }
            });
            if ('id' in conf) {
                this.addMetadata({ 'type': 'string', 'key': 'id', 'label': 'Name', 'value': conf['id'] });
            }
            this.readonly = conf['readonly'];
            this.locked = !!conf['locked'];
        }

        /**
         * Callback function that is called by the Metadata mixin class when
         * a metadata value has been changed (by the user).
         */
        onMetadataChange(key, value) {
            console.debug("Metadata change: '" + key + "' = " + value);
            if (key == 'type' || key == 'id') {
                // Change of datatype needs to be propagated back to parent node
                // since the whole object needs to be replaced
                this.parent.onSubnodeChange(this);
            }
        }

        /**
         * Virtual method that is  called from widgets/subnode whenever the
         * user has changed the value.
         *
         * @param {object} src The Widget object that made the call
         * @param {value} val The new value
         */
        onValueChange(src, val) {
            console.error("Virtual method not overloaded: \"onValueChange()\"");
        }

        /**
         * Virtual method that is called from parent when a new value has been
         * received from the backend.
         *
         * @param {array} data A SECoP-compatible data structure, i.e. an array
         *                     with two elements, the first being the value and
         *                     second a hash/object with options.
         */
        updateFromSource(data) {
            console.error("Virtual method not overloaded: \"updateFromSource()\"");
        }


        validateValue(data) {
            if (data.constructor !== Array) {
                throw "Received value is not in SECoP format - not an array with two elements";
            }
            if (data.length < 1 || data.length > 2) {
                throw "Received value is not in SECoP format - not an array with two elements";
            }
            if (data.length == 2 && typeof(data[1]) !== 'object') {
                throw "Received value is not in SECoP format - second element is not a hash";
            }
            return data;
        }

        /**
         * Draws the graphical representation of the node.
         *
         * @param {DOM-element} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            super.render(container);
            this.element.addClass('datainfo');
            if (this.locked) {
                this._toolbar_el.find('.edit-button').remove();
                this.element.addClass('locked');
            }
            // Hide delete in the context menu
            this._context_menu.context_menu('setItems', {
                    'delete': { 'hidden': true }
            });
        }
    }




    /**
     * @class DataInfoNode
     * @extends DataInfo
     *
     * Base class for all container-like Datainfo objects, i.e. SecArray,
     * SecStruct and SecTuple. These subclasses will not have their own
     * values and won't need showing widgets. Instead they act as containers
     * for other DataInfo objects.
     */
    class DataInfoNode extends DataInfo {
        /**
         * Callback that is called from child nodes indicating that a change has
         * occured that requires the parent to update the node structure.
         */
        onSubnodeChange() {
            this.parent.onSubnodeChange();
        }
    }




    /**
     * @class DataInfoValue
     * @extends DataInfo
     *
     * Base class for all  Datainfo objects that contains values and thus
     * needs to be able to handle and show Widgets.
     */
    class DataInfoValue extends DataInfo {
        /**
         * @constructor
         * Creates a new object with the configuration given as argument.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        constructor(conf) {
            super(conf);
            this._value = null;
            if (! this._widgets) {
                this._widgets = {};
            }
            if ('widgets' in conf && Array.isArray(conf['widgets'])) {
                // FIXME: Remove missing widgets
                for (let i in conf['widgets']) {
                    let type = conf['widgets'][i];
                    if (! (type in this._widgets)) {
                        this._widgets[type] = null;
                    }
                }
            }
        }

        /**
         * Converts the node to a simple heirarchial structure that can be
         * converted into JSON in SECoP format.
         *
         * @return {object} SECoP compatible object ready to be converted to JSON
         */
        serialize() {
            let structure = super.serialize();
            structure['widgets'] = this.widgetTypes();
            return structure;
        }

        /**
         * Returns a list of widget types that are rendered for this object.
         * Used for serialization.
         *
         * @return {array} A list of strings with names of the widget types
         */
        widgetTypes() {
            return Object.keys(this._widgets);
        }

        /**
         * Event callback that gets called when a widget toolbar item has been
         * dragged and dropped onto this DataInfo object. The function checks
         * that the dropped object is really a valid widget type. It then sets
         * the widget type to null to indicate that the widget should be created
         * when it calls createWidgets().
         *
         * @param {event} e Event object.
         * @param {DOM-element} ui The DOM-element that was dropped.
         */
        onDropWidgetType(e, ui) {
            e.stopPropagation();
            // Reset channel element position - we don't want to keep the element
            // here, just get a signal that we should make an Accessible from it.
            ui.draggable.css({'left': 0, 'top': 0});
            let widget_type = ui.draggable.data('type');
            console.debug("Add new widget type '" + widget_type + "' to '" + this.id + "'");
            if (! (widget_type in this._widgets)) {
                this._widgets[widget_type] = null;
            }
            this.dirty(true);
            this.createWidgets();
        }

        /**
         * Creates/updates the widgets for this DataInfo object. If the type in
         * the list doesn't match the type of the widget object then the object
         * is destroyed
         */
        createWidgets() {
            for (let type in this._widgets) {
                let widget = this._widgets[type];
                if (! widget) {
                    // Widget object doesn't exist so create it.
                    widget = this.createWidget(type);
                }
                else {
                    let actual_type = widget.constructor.name;
                    if (type != actual_type) {
                        // The widget type in the list doesn't match the actual object.
                        // This happens if the type has changed. Remove the current
                        // widget and create a new one of the correct type.
                        delete this._widgets[type];
                        widget.destroy();
                        widget = this.createWidget(type);
                    }
                }
                if (! widget) {
                    // Creation of widget failed
                    delete this._widgets[type];
                    return;
                }
                widget.render(this._nodes_el);
                this._widgets[type] = widget;
            }
        }

        /**
         * Creates a new widget object of the given type.
         *
         * @param {string} type Type id of the widget type to create.
         * @return {object} A widget object of the selected type with callback
         *                  bindings setup.
         */
        createWidget(type) {
            let conf = {
                'value': this.value(),
                'id': this.getMetadataValue('id'),
                'editable': !this.readonly,
                'show': true,
                'show label' : false,
                'change callback': this.onValueChange.bind(this),
                'delete callback': this.onDeleteWidget.bind(this),
                'min': this.getMetadataValue('min'),
                'max': this.getMetadataValue('max'),
                'unit': this.getMetadataValue('unit')
            };
            let members = this.getMetadataValue('members');
            if (members) {
                conf['members'] = JSON.parse(members);
            }
            console.debug("Creating new widget: " + type);
            let widget;
            try {
                widget = WidgetsPalette.createWidget(type, conf)
            }
            catch(err) {
                console.error("Could not create widget object for accessible " + this.parent.id + ": " + err);
                return null;
            }
            return widget;
        }

        /**
         * Callback function passed to the widgets that will then be called from the
         * widgets when the user clicks the delete button on that widget.
         *
         * @param {object} widget A reference to the widget object that called the function,
         *                        i.e. the widget that should be removed.
         */
        onDeleteWidget(widget) {
            for (let type in this._widgets) {
                if (this._widgets[type] === widget) {
                    widget.destroy();
                    delete this._widgets[type];
                    this.dirty(true);
                    return;
                }
            }
        }

        /**
         * Callback function that is called by the Metadata mixin class when
         * a metadata value has been changed (by the user).
         *
         * @param {string} key The id of the metadata field that was changed.
         * @param {value} value The value that the metadata field was changed to.
         */
        onMetadataChange(key, value) {
            super.onMetadataChange(key, value);
            // Create/update widgets
            this.createWidgets();
        }

        /**
         * Part of the NodeBase edit functionality. Called when the node is being
         * set into edit mode. This usually happens when the user clicks the edit
         * button for the node.
         */
        onEditStart() {
            super.onEditStart();
            for (let type in this._widgets) {
                let widget = this._widgets[type];
                if (widget) {
                    widget.showDeleteButton(true);
                }
            }
        }

        /**
         * Part of the NodeBase edit functionality. Called when the node is set to
         * no longer being editable. This usually happens when the user clicks the
         * edit button for the node.
         */
        onEditEnd() {
            for (let type in this._widgets) {
                let widget = this._widgets[type];
                if (widget) {
                    widget.showDeleteButton(false);
                }
            }
            super.onEditEnd();
        }

        /**
         * Returns the current value for the object.
         *
         * @return {object} The current value as whatever type the object uses.
         */
        value() {
            return this._value;
        }

        /**
         * This method is called from widgets whenever the user has changed
         * the value. This will just relay the new value to the parent node.
         *
         * @param (object) src The Widget object that made the call
         * @param (value) val The new value
         */
        onValueChange(src, val) {
            this._value = val;
            this.parent.onValueChange(this, val);
        }

        /**
         * Called from parent when a new value has been received from the backend.
         *
         * @param (Array) data A SECoP-compatible data structure, i.e. an array
         *                     with two elements, the first being the value and
         *                     second a hash/object with options.
         */
        updateFromSource(data) {
            this.validateValue(data);
            /*
            if (typeof(this._value) !== 'undefined' && this._value !== null && this.element) {
                this.element.removeClass('no-value');
            }
            */
            if (typeof(data[0]) === 'undefined') {
                return;
            }
            this._value = data[0];
            //this._qualifiers = data[1];
            if (this.getMetadataValue('fmtstr') && typeof(this._value) === "number") {
                let pat = /^%\.(\d+)(.)$/;
                let m = pat.exec(this.getMetadataValue('fmtstr'));
                if (m[2] === "g") {
                    this._value_string = this._value.toPrecision(m[1]);
                }
                else if (m[2] === "f") {
                    this._value_string = this._value.toFixed(m[1]);
                }
            }
            else {
                this._value_string = this._value + '';
            }
            if (this.getMetadataValue('unit')) {
                this._value_string = this._value_string + ' ' + this.getMetadataValue('unit');
            }
            for (let type in this._widgets) {
                let widget = this._widgets[type];
                if (widget) {
                    widget.value(this._value, this._value_string);
                }
            }
        }

        /**
         * Draws the graphical representation of the node.
         *
         * @param {DOM-element} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            super.render(container);
            this._nodes_el.addClass('widgets');
            this.createWidgets();
            /*
            if (typeof(this._value) === 'undefined' || this._value === null) {
                // No value has been received yet
                this.element.addClass('no-value');
            }
            */
            // Allow widgets to be dropped on the datainfo object
            if (! this.locked) {
                this.element.droppable({
                    'accept': '.widget-button',
                    'drop': this.onDropWidgetType.bind(this)
                });
            }
        }
    }



    /**
     * @class SecArray
     * @extends DataInfoValue
     *
     * Class for the SECoP 'array' datatype
     */
    // FIXME: We need special widget for arrays, or at least support for
    //        multiple values on update for graphs and other suitable widget.
    class SecArray extends DataInfoValue {
        /**
         * Updates the object with the representation sent from the backend in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        deserialize(conf) {
            this._maxlen_display_limit = 100;  // Max length of array for showing values as individual widgets
            super.deserialize(conf);
            if (! conf['minlen']) {
                conf['minlen'] = 0;
            }
            if (! conf['maxlen'] || conf['maxlen'] < 1) {
                conf['maxlen'] = 1;
            }
            this.addMetadata({ 'type': 'int', 'key': 'maxlen', 'label': 'Max number of values', 'value': conf['maxlen'] });
            this.addMetadata({ 'type': 'int', 'key': 'minlen', 'label': 'Min number of values', 'value': conf['minlen'] });
            let members_conf = conf['members'];
            if (! members_conf) {
                // If members have not been configured we default to Double
                members_conf = {
                    'type': 'double'
                };
            }
            members_conf['readonly'] = this.readonly;
            members_conf['parent'] = this;
            this._member_type = new classes[members_conf['type']](members_conf);
        }

        /**
         * Converts the node to a simple heirarchial structure that can be
         * converted into JSON in SECoP format.
         *
         * @return {object} SECoP compatible object ready to be converted to JSON
         */
        serialize() {
            let structure = super.serialize();
            structure['members'] = this._member_type.serialize();
            return structure;
        }

        /**
         * Event callback that gets called when a widget toolbar item has been
         * dragged and dropped onto this DataInfo object. The function checks
         * that the dropped object is really a valid widget type. It then sets
         * the widget type to null to indicate that the widget should be created
         * when it calls createWidgets().
         *
         * @param {event} e Event object.
         * @param {DOM-element} ui The DOM-element that was dropped.
         */
        onDropWidgetType(e, ui) {
            e.stopPropagation();
            // Reset channel element position - we don't want to keep the element
            // here, just get a signal that we should make an Accessible from it.
            ui.draggable.css({'left': 0, 'top': 0});
            let widget_type = ui.draggable.data('type');
            console.debug("Add new widget type '" + widget_type + "' to '" + this.id + "'");
            if (! (widget_type in this._members[0]._widgets)) {
                for (let i = 0; i < this._members.length; i++) {
                    this._members[i]._widgets[widget_type] = null;
                    this._members[i].createWidgets();
                }
            }
            this.dirty(true);
            //this.createWidgets();
        }


        /**
         * Creates/updates the widgets for this DataInfo object. If the type in
         * the list doesn't match the type of the widget object then the object
         * is destroyed
         */
        createWidgets() {
        }


        /**
         * Draws the graphical representation of the SecStruct object.
         *
         * @param {HTMLElement} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            super.render(container);
            this.element.addClass('sec-array');
            this._nodes_el.addClass('members');
            if (! this._table_el) {
                if (this.getMetadataValue('maxlen') <= this._maxlen_display_limit) {
                    this._table_el = $('<table><tr><th>No.</th><th>Value</th></tr></table>');
                    this._nodes_el.append(this._table_el);
                    for (let i = 0; i < this.getMetadataValue('maxlen'); i++) {
                        let row = $('<tr><td>' + i + '</td><td class="array-data"></td></tr>');
                        this._table_el.append(row);
                    }
                }
                else {
                    let maxlen = this.getMetadataValue('maxlen');
                    this._nodes_el.text("Array contains " + maxlen + " values - too many to display");
                }
            }
        }

        /**
         * Called from parent when a new value has been received from the backend.
         *
         * @param (Array) data A SECoP-compatible data structure, i.e. an array
         *                     with two elements, the first being the value and
         *                     second a hash/object with options.
         */
        updateFromSource(data) {
            this.validateValue(data);
            let values = data[0];
            let qualifiers = data[1];
            this._values = values;
            if (this._table_el) {
                let cells = this._table_el.find('.array-data');
                for (let i = 0; i < cells.length; i++) {
                    $(cells[i]).html(values[i] + '');
                }
            }
        }

        validateValue(data) {
            super.validateValue(data);
            let value = data[0];
            if (value.constructor !== Array) {
                throw "Invalid data received from source. Expected Array. Got " + typeof(value);
            }
            if (value.length < this.getMetadataValue('minlen')) {
                throw "Array value contains too few values. Min length is " +
                    this.getMetadataValue('minlen') + " elements. Got " +
                    value.length + " elements.";
            }
            if (value.length > this.getMetadataValue('maxlen')) {
                throw "Array value contains too many values. Max length is " +
                    this.getMetadataValue('maxlen') + " elements. Got " +
                    value.length + " elements.";
            }
            // TODO: Verify datatype of all elements
        }
    }



    /**
     * @class SecBool
     * @extends DataInfoValue
     *
     * Class for the SECoP 'bool' datatype
     */
    class SecBool extends DataInfoValue {
        /**
         * Updates the object with the representation sent from the backend in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        deserialize(conf) {
            super.deserialize(conf);
        }

        value() {
            // Always return a valid value even if not defined
            return !!this._value;
        }

        /**
         * This method is called from widgets whenever the user has changed
         * the value. This will just relay the new value to the parent node.
         *
         * @param (object) src The Widget object that made the call
         * @param (value) val The new value
         */
        onValueChange(src, val) {
            this._value = !!val;  // Make sure it's a bool
            this.parent.onValueChange(this, this._value);
        }

        validateValue(data) {
            super.validateValue(data);
            let value = data[0];
            if (typeof(value) !== 'boolean') {
                throw "Invalid data. Expected boolean. Got " + typeof(value);
            }
        }

        /**
         * Draws the graphical representation of the node.
         *
         * @param {HTMLElement} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            let created = super.render(container);
            if (created) {
                this.element.addClass('sec-bool');
            }
        }
    }


    /**
     * @class SecBlob
     * @extends DataInfoValue
     *
     * Class for the SECoP 'blob' datatype
     */
    class SecBlob extends DataInfoValue {
        /**
         * Updates the object with the representation sent from the backend in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        deserialize(conf) {
            super.deserialize(conf);
            this.addMetadata({ 'type': 'int', 'key': 'minbytes', 'label': 'Min number of bytes', 'value': conf['minbytes'] });
            this.addMetadata({ 'type': 'int', 'key': 'maxbytes', 'label': 'Max number of bytes', 'value': conf['maxbytes'] });
        }

        /**
         * Draws the graphical representation of the node.
         *
         * @param {HTMLElement} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            let created = super.render(container);
            if (created) {
                this.elemen.taddClass('sec-blob');
            }
        }
    }


    /**
     * @class SecDouble
     * @extends DataInfoValue
     *
     * Class for the SECoP 'double' datatype
     */
    class SecDouble extends DataInfoValue {
        /**
         * Updates the object with the representation sent from the backend in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        deserialize(conf) {
            super.deserialize(conf);
            this.addMetadata({ 'type': 'number', 'key': 'min', 'label': 'Min', 'value': conf['min'] });
            this.addMetadata({ 'type': 'number', 'key': 'max', 'label': 'Max', 'value': conf['max'] });
            this.addMetadata({ 'type': 'string', 'key': 'unit', 'label': 'Unit', 'value': conf['unit'] });
            this.addMetadata({ 'type': 'string', 'key': 'fmtstr', 'label': 'Format string', 'value': conf['fmtstr'] });
            this.addMetadata({ 'type': 'number', 'key': 'absolute_resolution', 'label': 'Absolute Resolution', 'value': conf['absolute_resolution'] });
            this.addMetadata({ 'type': 'number', 'key': 'relative_resolution', 'label': 'Relative Resolution', 'value': conf['relative_resolution'] });
        }

        /**
         * This method is called from widgets whenever the user has changed
         * the value. This will just relay the new value to the parent node.
         *
         * @param (object) src The Widget object that made the call
         * @param (value) val The new value
         */
        onValueChange(src, val) {
            this._value = parseFloat(val);
            this.parent.onValueChange(this, this._value);
        }

        validateValue(data) {
            super.validateValue(data);
            let value = data[0];
            if (typeof(value) !== 'number') {
                throw "Invalid data. Expected number. Got " + typeof(value) + ".";
            }
        }

        /**
         * Draws the graphical representation of the node.
         *
         * @param {HTMLElement} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            let created = super.render(container);
            if (created) {
                this.element.addClass('sec-double');
            }
        }
    }


    /**
     * @class SecEnum
     * @extends DataInfoValue
     *
     * Class for the SECoP 'enum' datatype
     */
    class SecEnum extends DataInfoValue {
        /**
         * Updates the object with the representation sent from the backend in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        deserialize(conf) {
            super.deserialize(conf);
            // FIXME: Needs special type of metadata widget
            let members = JSON.stringify(conf['members']);
            this.addMetadata({ 'type': 'string', 'key': 'members', 'label': 'Members', 'value': members });
        }

        /**
         * This method is called from widgets whenever the user has changed
         * the value. This will just relay the new value to the parent node.
         *
         * @param (object) src The Widget object that made the call
         * @param (value) val The new value
         */
        onValueChange(src, val) {
            this._value = parseInt(val);  // SECoP enums are always integers
            this.parent.onValueChange(this, this._value);
        }

        onMetadataChange(key, value) {
            super.onMetadataChange(key, value)
            if (key == 'members') {
                // A bit of a hack. Ideally only the widgets should be updated, but
                // that possibility has not been implemented yet.
                this.parent.onSubnodeChange(this)
            }
        }

        serialize() {
            let structure = super.serialize();
            structure['members'] = JSON.parse(structure['members']);
            return structure;
        }

        /**
         * Draws the graphical representation of the node.
         *
         * @param {HTMLElement} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            let created = super.render(container);
            if (created) {
                this.element.addClass('sec-enum');
            }
        }
    }


    /**
     * @class SecInt
     * @extends DataInfoValue
     *
     * Class for the SECoP 'int' datatype
     */
    class SecInt extends DataInfoValue {
        /**
         * Updates the object with the representation sent from the backend in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        deserialize(conf) {
            super.deserialize(conf);
            this.addMetadata({ 'type': 'int', 'key': 'min', 'label': 'Min', 'value': conf['min'] });
            this.addMetadata({ 'type': 'int', 'key': 'max', 'label': 'Max', 'value': conf['max'] });
            this.addMetadata({ 'type': 'string', 'key': 'unit', 'label': 'Unit', 'value': conf['unit'] });
        }

        /**
         * This method is called from widgets whenever the user has changed
         * the value. This will just relay the new value to the parent node.
         *
         * @param (object) src The Widget object that made the call
         * @param (value) val The new value
         */
        onValueChange(src, val) {
            this._value = parseInt(val);
            this.parent.onValueChange(this, this._value);
        }

        validateValue(data) {
            super.validateValue(data);
            let value = data[0];
            if (typeof(value) !== 'number' || value % 1 !== 0) {
                throw "Invalid data. Expected number. Got " + typeof(value) + ".";
            }
        }

        /**
         * Draws the graphical representation of the node.
         *
         * @param {HTMLElement} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            let created = super.render(container);
            if (created) {
                this.element.addClass('sec-int');
            }
        }
    }


    /**
     * @class SecScaled
     * @extends SecDouble
     *
     * Class for the SECoP 'scaled' datatype
     */
    // FIXME: Should display and get values in scaled format and rescale on publish/receive
    class SecScaled extends SecDouble {
        /**
         * Updates the object with the representation sent from the backend in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        deserialize(conf) {
            super.deserialize(conf);
            this.addMetadata({ 'type': 'number', 'key': 'scale', 'label': 'Scale', 'value': conf['scale'] });
        }

        /**
         * This method is called from widgets whenever the user has changed
         * the value. This will just relay the new value to the parent node.
         *
         * @param (object) src The Widget object that made the call
         * @param (value) val The new value
         */
        onValueChange(src, val) {
            this._value = parseInt(val);
            this.parent.onValueChange(this, this._value);
        }

        validateValue(data) {
            super.validateValue(data);
            let value = data[0];
            if (typeof(value) !== 'number') {
                throw "Invalid data. Expected number. Got " + typeof(value) + ".";
            }
        }

        /**
         * Draws the graphical representation of the node.
         *
         * @param {HTMLElement} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            let created = super.render(container);
            if (created) {
                this.element.addClass('sec-scaled');
            }
        }
    }


    /**
     * @class SecString
     * @extends DataInfoValue
     *
     * Class for the SECoP 'string' datatype
     */
    class SecString extends DataInfoValue {
        /**
         * Updates the object with the representation sent from the backend in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        deserialize(conf) {
            super.deserialize(conf);
            this.addMetadata({ 'type': 'int', 'key': 'minchars', 'label': 'Min number of characters', 'value': conf['minchars'] });
            this.addMetadata({ 'type': 'int', 'key': 'maxchars', 'label': 'Max number of characters', 'value': conf['maxchars'] });
            this.addMetadata({ 'type': 'bool', 'key': 'isUTF8', 'label': 'Is UTF8 encoded', 'value': conf['isUTF8'] });
        }

        value() {
            // Always return a valid value even if it is not defined
            if (! this._value) {
                return '';
            }
            else {
                return this._value;
            }
        }

        /**
         * This method is called from widgets whenever the user has changed
         * the value. This will just relay the new value to the parent node.
         *
         * @param (object) src The Widget object that made the call
         * @param (value) val The new value
         */
        onValueChange(src, val) {
            this._value = val + '';  // Guarantee the value is a string
            this.parent.onValueChange(this, this._value);
        }

        validateValue(data) {
            super.validateValue(data);
            let value = data[0];
            if (typeof(value) !== 'string') {
                throw "Invalid data. Expected string. Got " + typeof(value) + ".";
            }
        }

        /**
         * Draws the graphical representation of the SecString object.
         *
         * @param {HTMLElement} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            let created = super.render(container);
            if (created) {
                this.element.addClass('sec-string');
            }
        }
    }


    /**
     * @class SecStruct
     * @extends DataInfoNode
     *
     * Class for the SECoP 'struct' datatype
     */
    class SecStruct extends DataInfoNode {
        /**
         * Updates the object with the representation sent from the backend in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        deserialize(conf) {
            super.deserialize(conf);
            let widgets = {};
            if ('widgets' in conf && ! (conf['widgets'] instanceof Array) && typeof(conf['widgets']) === 'object') {
                widgets = conf['widgets'];
            }
            let optional = conf['optional'];
            if (! optional) {
                optional = [];
            }
            this.addMetadata({ 'type': 'string', 'key': 'optional', 'label': 'Optional', 'value': optional });
            if (!this._members) {
                this._members = {};
            }
            for (let member_id in conf['members']) {
                let member_conf = conf['members'][member_id];
                member_conf['readonly'] = this.readonly;
                member_conf['parent'] = this;
                member_conf['id'] = member_id;
                if (member_id in widgets) {
                    member_conf['widgets'] = widgets[member_id];
                }
                if (member_id in this._members) {
                    // Member already exists - just update it
                    this._members[member_id].update(member_conf);
                }
                else {
                    // New member
                    this._members[member_id] = new classes[member_conf['type']](member_conf);
                }
                // Store the id in the object to use on callbacks. We can't use the
                // id we send in to the constructor as it is a metadata value
                // and can therefore be changed by the user.
                this._members[member_id].member_id = member_id;
            }
            // TODO: On update remove members that no longer exists
        }

        /**
         * Converts the node to a simple heirarchial structure that can be
         * converted into JSON in SECoP format.
         *
         * @return {object} SECoP compatible object ready to be converted to JSON
         */
        serialize() {
            let structure = super.serialize();
            let widgets = {};
            structure['members'] = {}
            for (let member_id in this._members) {
                let member_structure = this._members[member_id].serialize();
                let actual_id = member_structure['id'];
                delete member_structure['id'];
                widgets[actual_id] = member_structure['widgets'];
                delete member_structure['widgets'];
                structure['members'][actual_id] = member_structure
            }
            structure['widgets'] = widgets;
            return structure;
        }

        /**
         * Draws the graphical representation of the SecStruct object.
         *
         * @param {HTMLElement} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            super.render(container);
            this.element.addClass('sec-struct');
            this._nodes_el.addClass('members');
            // Add "add member" button
            if (this._toolbar_el.find('.add-member-button').length === 0) {
                let add_member_button = $('<button class="add-member-button">Add member</button>');
                add_member_button.on('click', this.onAddMemberClick.bind(this));
                this._toolbar_el.append(add_member_button);
            }
            for (let member_id in this._members) {
                let member = this._members[member_id];
                member.setMetadataValue('id', member_id);
                member.render(this._nodes_el);
                member.element.addClass('member');
            }
        }

        /**
         * Returns a list of widgets that are rendered for this object.
         * Used for serialization. For Structs this is recursive and builds a
         * (tree) structure for all members.
         */
        widgetTypes() {
            let structure = {}
            for (let id in this._members) {
                let member = this._members[id];
                structure[id] = member.widgetTypes();
            }
            return structure;
        }


        onEditStart() {
            super.onEditStart();
            for (let id in this._members) {
                let member = this._members[id];
                member.onEditStart();
            }
        }

        onEditEnd() {
            for (let id in this._members) {
                let member = this._members[id];
                member.onEditEnd();
            }
            super.onEditEnd();
        }

        /**
         * Called from parent when a new value has been received from the backend.
         *
         * @param (Array) data A SECoP-compatible data structure, i.e. an array
         *                     with two elements, the first being the value and
         *                     second a hash/object with options.
         */
        updateFromSource(data) {
            this.validateValue(data);
            let values = data[0];
            let qualifiers = data[1];
            for (let member_id in values) {
                if (! (member_id in this._members)) {
                    console.error("Struct has no member '" + member_id + "'");
                }
                else {
                    this._members[member_id].updateFromSource([values[member_id], qualifiers]);
                }
            }
        }

        /**
         * This method is called from sub-nodes whenever the user has changed
         * the value. This will collect all current values from sub-nodes and
         * create a new complete value strcuture with the new value inserted,
         * and then send the full value structure to its parent's onValueChange method.
         *
         * @param (object) src The node object that made the call
         * @param (value) val The new value
         */
        onValueChange(src, val) {
            let values = {};
            for (let member_id in this._members) {
                let member = this._members[member_id];
                values[member_id] = member.value();
            }
            this.parent.onValueChange(this, values);
        }

        validateValue(data) {
            super.validateValue(data);
            let value = data[0];
            if (typeof(value) !== 'object') {
                throw "Invalid data. Expected object. Got " + typeof(value) + ".";
            }
            let optional = this.getMetadataValue('optional');
            for (let member_id in this._members) {
                if (! (member_id in value) && ! (member_id in optional)) {
                    throw "Invalid data. Non-optional key is missing: " + member_id;
                }
            }
        }

        /**
         * Event callback that gets called when a widget toolbar item has been
         * dragged and dropped onto this DataInfo object. The function checks
         * that the dropped object is really a valid widget type. It then sets
         * the widget type to null to indicate that the widget should be created
         * when it calls createWidgets().
         *
         * @param {event} e Event object.
         * @param {DOM-element} ui The DOM-element that was dropped.
         */
        onDropWidgetType(e, ui) {
            // Structs don't allow widgets directly so we just reset
            // the draggable object and ignore the event.
            e.stopPropagation();
            ui.draggable.css({'left': 0, 'top': 0});
        }

        /**
         * Event callback for clicks on the "Add Member" button. The callback link
         * is setup in the render() method. It will add a new member to the struct.
         * By default it creates a new SecDouble.
         *
         * @param {event} e Event object.
         */
        onAddMemberClick(e) {
            let member_id = 'Member-' + Date.now();
            let member_conf = {
                'id': member_id,
                'type': 'double',
                'parent': this
            }
            console.debug("New blank member: " + member_id);
            let new_member = factory(member_conf);
            this._members[member_id] = new_member;
            this.dirty(true);
            new_member.render(this._nodes_el);
            // Set the new data member in edit mode
            new_member.onEditStart();
        }

        /**
         * Sets or returns the dirty flag i.e. the indicator that the node has been changed
         * by the user and should be saved. If a bool value is applied as argument the
         * state is set to that value. If no argument it supplied the current state is returned
         * and no change is made.
         *
         * @param {bool} state If defined the dirty flag will be set to this value.
         * @return {bool} True if changes have been made, otherwise false.
         */
        dirty(state) {
            if (typeof(state) !== 'undefined') {
                // Set dirty state
                super.dirty(state);
                if (! this._dirty) {
                    // Clear dirty flag on all members
                    for (let member_id in this._members) {
                        this._members[member_id].dirty(false);
                    }
                }
                return this._dirty;
            }
            // Don't set state, just return current state
            if (super.dirty()) {
                return true;
            }
            for (let member_id in this._members) {
                if (this._members[member_id].dirty()) {
                    return true;
                }
            }
            return false;
        }
    }


    /**
     * @class SecTuple
     * @extends DataInfoNode
     *
     * Class for the SECoP 'tuple' datatype
     *
     * @todo Not yet implemented
     */
    class SecTuple extends DataInfoValue {
        /**
         * Updates the object with the representation sent from the backend in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        deserialize(conf) {
            super.deserialize(conf);
            this._members = conf['members'];
        }

        validateValue(data) {
            super.validateValue(data);
            let value = data[0];
            if (value.constructor !== Array) {
                throw "Invalid data. Expected Array. Got " + typeof(value) + ".";
            }
            if (value.length != 2) {
                throw "Invalid data. Tuple must have exactly two values. Got " + value.length + " values.";
            }
        }

        /**
         * Draws the graphical representation of the node.
         *
         * @param {HTMLElement} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            let created = super.render(container);
            if (created) {
                this.element.addClass('sec-tuple');
            }
        }
    }



    /**
     * Conversion table between SECoP type string to class
     */
    let classes = {
        'datainfo': DataInfo,
        'scaled': SecScaled,
        'int': SecInt,
        'double': SecDouble,
        'bool': SecBool,
        'enum': SecEnum,
        'string': SecString,
        'blob': SecBlob,
        'array': SecArray,
        'tuple': SecTuple,
        'struct': SecStruct
    };


    /**
     * Creates a new DataInfo object of the appropriate type according to
     * the SECoP data type.
     *
     * @param {object} Representation of the datainfo object to be created.
     */
    function factory(conf) {
        let type = conf['type']
        if (! (type in classes)) {
            throw("Unsupported data type: " + type);
        }
        let datainfo = new classes[type](conf);
        return datainfo;
    }

    return factory;
});
