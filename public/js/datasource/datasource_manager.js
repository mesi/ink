/**
 * @file datasource_manager.js
 * @author Andreas Hagelberg
 *
 * Implements the Data Source Manager module
 */


'use strict';

define(['jquery', 'events', 'octopy', 'json2', 'modes', 'edit_panel', 'forms/datasource_editor',
        'forms/channel_editor', 'datasource/datasource', 'datasource/channel'],
       function($, Events, Octopy, json, Modes, EditPanel, DataSourceEditor, ChannelEditor,
                DataSource, Channel)
{

    /**
     * @class DataSourceManager
     *
     * The Data Source Manager handles the creation and configuration of
     * data sources and channels. It directly interacts with the data source
     * manager backend module and data source gateways.
     */
    class DataSourceManager {
        constructor() {
            this._datasources = {};
            this._channels = {};
            this._subscribed = false;
            let conf = {
                'datasource id': 'unknown'
            }
            this.addDataSource(conf);
        }


        /**
         * Starts the module. Creates the widget and add a button to the toolbar.
         */
        start() {
            console.debug("Starting data source manager");
            this.render();
            $(Modes).on('change', this.onModeChange.bind(this));
        }


        /**
         * Creates the DOM elements for the data source manager.
         *
         * @param {DOM-element} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            if (this.widget) {
                return;
            }
            let html = `
                <div id="datasource-manager">
                    <div class="title">Data Sources</div>
                    <button id="add-datasource">Add Data Source</button>
                    <div class="datasources"></div>
                </div>`;
            this.widget = $(html);
            this._datasources_list = this.widget.find('.datasources');
            this.widget.find('button#add-datasource').click(this.newDataSource);
            EditPanel.addModule(this.widget);
        }


        /**
         * Callback for mode change events
         *
         * @param {event} e Event object
         * @param {string} mode The mode that changed state
         * @param {bool} state The active state of the mode - true = active, false = inactive.
         */
        onModeChange(e, mode, state) {
            if (mode === 'edit' && state) {
                this.onEditStart();
            }
            else if(mode == 'edit' && !state) {
                this.onEditEnd();
            }
        }


        /**
         * Called when edit mode is activated. Starts listening to relevant
         * messages from the backend.
         */
        onEditStart() {
            if (! this._subscribed) {
                /* Subscribe to data source events on first edit
                 * We do this here instead of at start because the server config will
                 * take a bit of time to load and may not be available directly on start.
                 */
                let status_topic = Octopy.topics['datasource']['status'] + '/#';
                let channel_topic = Octopy.topics['datasource']['links'] + '/#';
                Events.subscribe(status_topic, this.onDataSourceStatusMessage.bind(this));
                Events.subscribe(channel_topic, this.onChannelMessage.bind(this));
                this._subscribed = true;
                this.renderDataSourceList();
            }
        }


        /**
         * Called when edit mode is deactivated. Does nothing currently.
         */
        onEditEnd() {
        }


        /**
         * Callback for data source list messages from the backend.
         * It updates the data source list in the GUI based on the message.
         *
         * @param {object} e The message containing topic and payload.
         */
        onDataSourceStatusMessage(e) {
            let source_id = e.topic.split('/').pop();
            if (! e.payload || e.payload == '' || e.payload == '[]') {  // '[]' is not a valid value, but a bug in the TCP Datasource makes it respond this sometime
                // Empty message means the datasource is no longer active
                // and should be removed
                if (this._datasources[source_id]) {
                    // Datasource exists and so it should be removed
                    let datasource = this._datasources[source_id];
                    datasource.destroy();;
                    delete this._datasources[source_id];
                }
            }
            else {
                let source_conf = JSON.parse(e.payload);
                source_conf['status topic'] = e.topic;
                if (this._datasources[source_id]) {
                    // Datasource already exists - update
                    let datasource = this._datasources[source_id];
                    datasource.update(source_conf);
                }
                else {
                    // New datasource
                    this.addDataSource(source_conf);
                }
                this.renderDataSourceList();
            }
        }


        /**
         * Callback for channels list messages from the backend.
         * It updates the channels list in the GUI based on the message.
         *
         * @param {object} e The message containing topic and payload.
         */
        onChannelMessage(e) {
            let datasource_id = e.topic.split('/').pop();
            let datasource;
            if (datasource_id && (datasource_id in this._datasources)) {
                datasource = this._datasources[datasource_id];
            }
            else {
                // FIXME: A new datasource should be added instead.
                datasource = this._datasources['unknown'];
            }
            if (! e.payload || e.payload == '') {
                // Empty message means all channels should be removed
                // Probably the datasource has just been removed
                datasource.clearChannels();
            }
            else {
                let channels_conf = JSON.parse(e.payload);
                let new_list = [];
                $.each(channels_conf, function(i, channel_conf) {
                    datasource.addChannel(channel_conf);
                    new_list.push(channel_conf['channel id']);
                }.bind(this));
                datasource.trimChannels(new_list);
                this.renderDataSourceList();
                Octopy.requestDataUpdate();
            }
        }


        /**
         * (Re-)draws the data source list.
         */
        renderDataSourceList() {
            $.each(this._datasources, function(source_id, source) {
                source.render(this._datasources_list);
            }.bind(this));
        }


        /**
         * Adds a new data source to the data source list.
         *
         * @param {object} conf Configuration for the data source - sent to the
         *                      constructor of the DataSource constructor.
         */
        addDataSource(conf) {
            let source_id = conf['datasource id'] || conf['hardware id'];
            let status_topic = conf['status topic']
            let type = conf['type'];
            if (! (type in DataSource)) {
                type = 'unknown';
            }
            conf['add channel callback'] = this.newChannel.bind(this);
            conf['delete datasource callback'] = this.deleteDataSource.bind(this);
            if ((source_id in this._datasources) && (this._datasources[source_id] instanceof DataSource[type])) {
                // Existing data source object is of the correct type so we
                // only need to update
                this._datasources[source_id].update(conf);
            } else {
                console.debug("New datasource (" + type + "): '" + source_id + "'");
                let new_datasource = new DataSource[type](conf);
                if (source_id in this._datasources) {
                    // DataSource already exists, but is of the wrong type - it was probably
                    // pre-loaded as basic non-specific DataSource object. We need to
                    // transfer channels to the new object.
                    console.warning("Datasource type does not match: old is '" +
                                  this._datasources[source_id] + "', new is '" +
                                  DataSource[type] + "'.");
                    new_datasource.channels = this._datasources[source_id].channels;
                    this.removeDataSource(id);
                }
                this._datasources[source_id] = new_datasource;
            }
            return this._datasources[source_id];
        }


        /**
         * Button callback that shows the dialog for creating a new data source
         * to the user.
         */
        newDataSource() {
            let conf = {
                "title": "Add Data Source",
                "done button": "Add"
            };
            if (! this.datasource_editor) {
                this.datasource_editor = new DataSourceEditor(conf);
            }
            this.datasource_editor.open(conf);
        }


        /**
         * Button callback that shows the dialog for creating a new channel
         * to the user.
         */
        newChannel(conf) {
            if (! this.channel_editor) {
                this.channel_editor = new ChannelEditor(conf);
            }
            conf['title'] = 'Add Channel';
            conf['done button'] = 'Add';
            this.channel_editor.open(conf);
        }


        /**
         * Callback for deleting a data source. It will send back a delete
         * command to the backend, but not remove it from the GUI. This will
         * happen when the new data source list is received from the backend.
         *
         * @param {object} conf Information about the data source to delete.
         * @param {string} conf['datasource id'] ID of the data source to delete.
         */
        deleteDataSource(conf) {
            let source_id = conf['datasource id'];
            console.debug("Delete data source: " + source_id);
            let remove_topic = Octopy.topics['datasource']['remove source'];
            let payload = {
                'datasource id': source_id
            }
            Events.publish(remove_topic, payload);
            let datasource = this._datasources[source_id];
            datasource.destroy();
        }


        /**
         * Removes a data source from the data source list.
         */
        removeDataSource(source_id) {
            if (source_id in this._datasources) {
                console.debug("Removing data source: '" + source_id + "'");
                let datasource = this._datasources[source_id]
                let status_topic = datasource.status_topic;
                datasource.destroy();
                delete this._datasources[source_id];
            }
        }

    }


    return new DataSourceManager();
});

