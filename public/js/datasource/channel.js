/**
 * @file channel.js
 * @author Andreas Hagelberg
 *
 * Implements the Channel class
 */


'use strict';

define(['jquery', 'events', 'octopy', 'datasource/datasource', 'secop/datainfo', 'secop/node_base', 'context_menu'],
       function($, Events, Octopy, DataSource, DataInfoFactory, NodeBase, _)
{

    /**
     * @class Channel
     * @extends NodeBase
     *
     * Class representing a channel, ie a data flow to/from a data source.
     */
    class Channel extends NodeBase {
        /**
         * @param {object} conf Channel configuration hash received from
         *                      the backend.
         */
        constructor(conf) {
            super(conf);
            this.deleteChannelCallback = conf['delete channel callback'];
        }


        /**
         * Updates the object with the representation sent from the backend in
         * JSON format and updates the data link.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        update(conf) {
            if (! this._editing) {
                this.deserialize(conf);
                this.link();
            }
        }


        /**
         * Updates the object with the representation sent from the backend in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        deserialize(conf) {
            conf['id'] = conf['channel id'];
            super.deserialize(conf);
            this.datasource_id = conf['datasource id'];
            this.addMetadata({ 'type': 'string', 'key': 'id', 'label': 'ID', 'value': conf['id'], 'readonly': true });
            this.addMetadata({ 'type': 'string', 'key': 'description', 'label': 'Description', 'value': conf['description'] });
            this.addMetadata({ 'type': 'bool', 'key': 'readonly', 'label': 'Read-only', 'value': !!conf['readonly'], 'readonly': true });
            this.addMetadata({ 'type': 'string', 'key': 'value_topic', 'label': 'Value topic', 'value': conf['value topic'], 'readonly': true });
            this.addMetadata({ 'type': 'string', 'key': 'get_topic', 'label': 'Get topic', 'value': conf['get topic'] });
            this.addMetadata({ 'type': 'string', 'key': 'set_topic', 'label': 'Set topic', 'value': conf['set topic'] });
            this.addMetadata({ 'type': 'string', 'key': 'get_reference', 'label': 'Get reference', 'value': conf['get reference'] });
            this.addMetadata({ 'type': 'string', 'key': 'set_reference', 'label': 'Set reference', 'value': conf['set reference'] });
            this.addMetadata({ 'type': 'string', 'key': 'value', 'label': 'Value', 'readonly': true });
            if (('datainfo' in conf) && ('type' in conf['datainfo'])) {
                var datainfo_conf = conf['datainfo'];
                this.deserializeDatainfo(datainfo_conf);
            }
        }


        /**
         * Helper method for converting the datainfo part of the JSON representation
         * sent from the backend.
         *
         * @param {object} conf Representation of the datainfo converted from JSON.
         */
        deserializeDatainfo(conf) {
            // FIXME: Update datainfo if types differ
            if (! this.datainfo) {
                var type = conf['type'];
                conf['parent'] = this;
                this.datainfo = DataInfoFactory(conf);
            }
        }


        /**
         * Converts the channel to a simple heirarchial structure that can be
         * converted into JSON format.
         *
         * @return {object} JSON representation of the channel object
         */
        serialize() {
            let structure = {
                'channel id': this.getMetadataValue('id'),
                'datasource id': this.datasource_id,
                'readonly': this.getMetadataValue('readonly'),
                'description': this.getMetadataValue('description'),
                'value topic': this.getMetadataValue('value_topic'),
                'get topic': this.getMetadataValue('get_topic'),
                'set topic': this.getMetadataValue('set_topic'),
                'get reference': this.getMetadataValue('get_reference'),
                'set reference': this.getMetadataValue('set_reference')
            }
            if (this.datainfo) {
                structure['datainfo'] = this.datainfo.serialize();
            }
            return structure;
        }


        /**
         * Links the accessible to the data-flow by subscribing to the relevant topics.
         * The data is received by the onMessage() callback function.
         */
        link() {
            if (this.getMetadataValue('value_topic')) {
                Events.subscribe(this.getMetadataValue('value_topic'), this);
            }
            // Request new data in case the value_topic is slow or not sent out automatically.
            Octopy.requestDataUpdate();
        }

        /**
         * Uninks the accessible to the data-flow.
         */
        unlink() {
            if (this.getMetadataValue('value_topic')) {
                Events.unsubscribe(this.getMetadataValue('value_topic'), this);
            }
        }

        /**
         * Event message callback function. This function is called by the Events
         * module when a new message matching the subscription is received from the
         * backend (from the MQTT server).
         *
         * @param {object} msg The event message object containing topic and payload.
         */
        onMessage(msg) {
            if (! msg.payload) {
                console.info("Empty data message for topic: " + msg.topic);
                return false;
            }
            try {
                var data = JSON.parse(msg.payload);
            }
            catch(e) {
                console.error("Error parsing JSON message for topic: " + msg.topic);
                console.error(e);
                console.error(msg.payload);
                return false;
            }
            if (data && data.constructor === Array && data.length > 0) {
                let value = data[0];
                if (typeof(value) !== 'undefined') {
                    value = value + '';
                    // Show value as string. Truncate to max 32 characters.
                    this.setMetadataValue('value', value.substring(0, 32));
                }
            }
        }


        /**
         * Event handler for the context-menu
         */
        onMenuClick(e, item) {
            super.onMenuClick(e, item);
            switch(item) {
            case 'delete':
                this.deleteChannelCallback({'channel id': this.id});
                break;
            }
        }


        /**
         * Helper method for the onModeChange callback for when the "edit" mode
         * is activated.
         */
        onEditStart() {
            super.onEditStart();
            this._context_menu.context_menu('setItems', {
                'edit': { 'title': 'Save' }
            });
        }


        /**
         * Helper method for the onModeChange callback for when the "edit" mode
         * is deactivated. If any changes have been made to the channel by the
         * user it will send the changes to the backend.
         */
        onEditEnd() {
            super.onEditEnd();
            if (this.dirty()) {
                let topic = Octopy.topics['datasource']['set channels'];
                let structure = [ this.serialize() ];
                console.debug('Saving channel ' + this.id);
                Events.publish(topic, structure);
                this.dirty(false);
            }
        }


        /**
         * Creates the DOM elements for the Channel object
         *
         * @param {HTMLElement} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            super.render(container);
            this.element.addClass('channel');
            // Store reference to Channel object in element so it can be accessed
            // during drag-and-drop operations (only the DOM element is being tracked
            // during drag-and-drop).
            this.element.data({'channel': this});
            this.element.draggable({
                'snap': '.module',
                'revert': 'invalid',
                'helper': 'clone',
                'appendTo': 'body'
            });
        }



        /**
         * Destroys the graphical representation of the channel.
         */
        destroy() {
            this.unlink();
            super.destroy();
        }
    }

    return Channel;
});
