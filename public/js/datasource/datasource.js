/**
 * @file datasource.js
 * @author Andreas Hagelberg
 *
 * Implements the DataSource class
 */

'use strict';

define(['jquery', 'jquery-ui', 'events', 'octopy', 'datasource/channel', 'secop/node_base'], function($, _, Events, Octopy, Channel, NodeBase) {

    /**
     * @class DataSource
     *
     * The DataSource class represents a data source in the backend.
     * A datasource is a piece of hardware or a program that generates
     * or accepts data signals.
     */
    class DataSource extends NodeBase {
        /**
         * @param {object} conf Datasource configuration hash received from
         *                      the backend.
         */
        constructor(conf) {
            super(conf);
            this.addChannelCallback = conf['add channel callback'];
            this.deleteDataSourceCallback = conf['delete datasource callback'];
        }


        /**
         * Updates the object with the representation sent from the backend
         * in JSON format.
         *
         * @param {object} conf Representation of the object converted from JSON.
         */
        update(conf) {
            this.deserialize(conf);
        }


        /**
         * Creates the object with the representation sent from the backend
         * in JSON format.
         *
         * @param {object} conf Representation of the object.
         */
        deserialize(conf) {
            if (! ('datasource id' in conf) || !conf['datasource id']) {
                throw("Missing id for datasource");
            }
            this.status_topic = conf['status topic'];
            conf['id'] = conf['datasource id'];
            conf['parent'] = null;
            this.type = conf['type'];
            if (! this._channels) {
                this._channels = {};
            }
            super.deserialize(conf);
            this.addMetadata({ 'type': 'string', 'key': 'id', 'label': 'ID', 'value': conf['id'], 'readonly': true });
            this.addMetadata({ 'type': 'string', 'key': 'status', 'label': 'Status', 'value': conf['status'], 'readonly': true });
            if ('links' in conf) {
                let keep_list = [];
                $.each(conf['links'], function(i, channel_conf) {
                    this.addChannel(channel_conf);
                    keep_list.push(channel_conf['channel id']);
                }.bind(this));
                this.trimChannels(keep_list);
            }
        }


        /**
         * Add a new channel to the data source.
         *
         * @param {object} channel_conf Configuration for the channel received
         *                              from the backend.
         */
        addChannel(channel_conf) {
            let channel_id = channel_conf['channel id'];
            channel_conf['parent'] = this;
            if (! this._channels) {
                this._channels = {};
            }
            if (! (channel_id in this._channels)) {
                // New channel - create it
                console.debug("New channel: '" + channel_id + "'");
                channel_conf['delete channel callback'] = this.deleteChannel.bind(this);
                let new_channel = new Channel(channel_conf);
                this._channels[channel_id] = new_channel;
            }
            else {
                // Existing channel - update it
                this._channels[channel_id].update(channel_conf);
            }
        }


        /**
         * Remove old channels no longer available in the backend.
         *
         * @param {array} keep_list A list of all channel id:s that are valid,
         *                          any channel not found in this list will be
         *                          removed.
         */
        trimChannels(keep_list) {
            $.each(this._channels, function(id, channel) {
                if (! keep_list.includes(id)) {
                    this.removeChannel(id);
                }
                if (channel.datasource_id != this.id) {
                    this.removeChannel(id);
                }
            }.bind(this));
        }


        /**
         * Remove a channel from the data source
         *
         * @param {string} channel_id ID of the channel to remove.
         */
        removeChannel(channel_id) {
            if (channel_id in this._channels) {
                console.debug("Removing channel from source '" + this.id + "': '" + channel_id + "'");
                this._channels[channel_id].destroy();
                delete this._channels[channel_id];
            }
        }


        /**
         * Remove all channels from datasource
         */
        clearChannels() {
            $.each(this._channels, function(id, channel) {
                channel.destroy();
            });
            this._channels = {};
        }


        /**
         * Callback for deleting a channel. It will send back a delete
         * command to the backend, but not remove it from the GUI. This will
         * happen when the new channels list is received from the backend.
         *
         * @param {object} conf Information about the channel to delete
         */
        deleteChannel(conf) {
            let channel_id = conf['channel id'];
            console.debug("Delete channel: " + channel_id);
            let remove_topic = Octopy.topics['datasource']['remove channels'];
            let payload = [ {
                'channel id': channel_id,
                'datasource id': this.id
            } ];
            Events.publish(remove_topic, payload);
        }


        /**
         * Event handler for the context-menu
         */
        onMenuClick(e, item) {
            super.onMenuClick(e, item);
            switch(item) {
            case 'add channel':
                this.addChannelCallback({'datasource id': this.id});
                break;

            case 'delete':
                this.deleteDataSourceCallback({'datasource id': this.id});
                break;
            }
        }


        /**
         * Creates the DOM elements for the Datasource object
         *
         * @param {HTMLElement} container The DOM element that the node element
         *                                should be placed in.
         */
        render(container) {
            super.render(container);
            if (!this.value_el) {
                this.element.addClass('datasource');
                this._nodes_el.addClass('channels');
                // Setup context menu
                let hide_add = (this.type == "mqtt" || this.type == 'unknown');
                this._context_menu.context_menu({
                    'items': {
                        'add channel': { 'title': 'Add channel', 'hidden': hide_add }
                    }
                });
            }
            this.renderChannelList();
        }


        /**
         * (Re-)draws the channel list.
         */
        renderChannelList() {
            $.each(this._channels, function(id, channel) {
                channel.render(this._nodes_el);
            }.bind(this));
        }


        /**
         * Helper function for render() which does the actual creation
         * of the DOM elements.
         */
        createWidget() {
            var html = `
                <div class="datasource">
                    <div class="head">
                        <div class="icon"></div>
                        <div class="name" id="name"></div>
                        <div class="status"></div>
                        <button class="delete-datasource" title="Delete Datasource"></button>
                    </div>
                    <button class="add-channel">Add Channel</button>
                    <div class="channels"></div>
                </div>`;
            var widget = $(html);
            return widget;
        }


        /**
         * Destroys the graphical representation of the data source. It will
         * first destroy all channels and then the data source itself.
         */
        destroy() {
            $.each(this.channels, function(ch_id, channel) {
                channel.destroy();
            });
            if (this.element) {
                this.element.remove();
            }
            this.element = null;
        }
    }




    /***************************
     * DefaultDataSource class *
     ***************************/
    class DefaultDataSource extends DataSource {
        render(container) {
            super.render(container);
            this.element.find('.add-channel').hide();
        }

        addChannel(channel_conf) {
            super.addChannel(channel_conf);
            this.hideIfEmpty();
        }

        removeChannel(channel_conf) {
            super.removeChannel(channel_conf);
            this.hideIfEmpty();
        }

        trimChannels(channel_list) {
            super.trimChannels(channel_list);
            this.hideIfEmpty();
        }

        hideIfEmpty() {
            if (this._channels.length > 0 && this.element) {
                this.element.show();
            }
            else if (this.element) {
                this.element.hide();
            }
        }

        render(container) {
            super.render(container);
            this.hideIfEmpty();
            this._context_menu.context_menu({
                'items': {
                    'delete': { 'hidden': true }
                }
            });
        }
    }


    /*************************
     * EpicsDataSource class *
     *************************/
    class EpicsDataSource extends DataSource {
        deserialize(conf) {
            super.deserialize(conf);
            this.name = 'EPICS';
        }

        render(container) {
            super.render(container);
            this._context_menu.context_menu({
                'items': {
                    'delete': { 'hidden': true },
                    'edit': { 'hidden': true }
                }
            });
        }
    }


    /**************************
     * SystemDataSource class *
     **************************/
    class SystemDataSource extends DataSource {
        deserialize(conf) {
            super.deserialize(conf);
            this.name = 'System';
        }

        render(container) {
            super.render(container);
            this._context_menu.context_menu({
                'items': {
                    'delete': { 'hidden': true },
                    'add channel': { 'hidden': true },
                    'edit': { 'hidden': true }
                }
            });
        }
    }


    /*****************************
     * SequencerDataSource class *
     *****************************/
    class SequencerDataSource extends DataSource {
        deserialize(conf) {
            super.deserialize(conf);
            this.name = 'Sequencer';
        }

        render(container) {
            super.render(container);
            this._context_menu.context_menu({
                'items': {
                    'delete': { 'hidden': true },
                    'add channel': { 'hidden': true },
                    'edit': { 'hidden': true }
                }
            });
        }
    }


    /***********************
     * MQTTDataSource class *
     ***********************/
    class MQTTDataSource extends DataSource {
        deserialize(conf) {
            super.deserialize(conf);
            this.prefix = conf['prefix'];
        }

        render(container) {
            super.render(container);
            this._context_menu.context_menu({
                'items': {
                    'add channel': { 'title': 'Add channel', 'hidden': false }
                }
            });
        }
    }


    /**************************
     * SocketDataSource class *
     **************************/
    class SocketDataSource extends DataSource {
        deserialize(conf) {
            super.deserialize(conf);
            this.host = conf['host'];
            this.port = conf['port'];
            this.delimiter = conf['delimiter'];
            this.pollinterval = conf['pollinterval'];
        }
    }



    /**************************
     * SerialDataSource class *
     **************************/
    class SerialDataSource extends DataSource {
        deserialize(conf) {
            super.deserialize(conf);
            this.port = conf['port'];
            this.baudrate = conf['baudrate'];
            this.databits = conf['databits'];
            this.stopbits = conf['stopbits'];
            this.parity = conf['parity'];
            this.delimiter = conf['delimiter'];
            this.pollinterval = conf['pollinterval'];
        }
    }


    var classes = {
        'datasource': DataSource,
        'unknown': DefaultDataSource,
        'epics': EpicsDataSource,
        'system': SystemDataSource,
        'mqtt': MQTTDataSource,
        'socket': SocketDataSource,
        'serial': SerialDataSource,
        'sequencer': SequencerDataSource
    };

    return classes;
});
