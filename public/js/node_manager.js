/**
 * @file node_manager.js
 * @author Andreas Hagelberg
 *
 * Implements the Node Manager module
 */


'use strict';

define(['jquery', 'events', 'octopy', 'json2', 'modes', 'edit_panel',
        'secop/secnode', 'forms/node_editor'],
       function($, Events, Octopy, json, Modes, EditPanel, SECNode, NodeEditor)
{

    /**
     * @class NodeManager
     *
     * The Node Manager handles the creation and configuration of
     * nodes, modules and accessibles.
     */
    class NodeManager {
        constructor() {
            this.hardware = {};
            this.nodes = {};
            $(Modes).on('change', this.onModeChange.bind(this));
        }


        /**
         * Returns the number of nodes.
         *
         * @return {int} Number of nodes.
         */
        node_count() {
            return Object.keys(this.nodes).length;
        }


        /**
         * Starts the module. Creates the widget and listens to structure messages
         */
        start() {
            console.debug("Starting node manager");
            this.node_list = $('#secnodes');
            this.render();

            $(Octopy).on('ready', function(e) {
                // Subscribe to node structure events
                Events.subscribe(Octopy.topics['node manager']['structure'], this.onStructureMessage.bind(this));
            }.bind(this));
            /*
            $(Modes).on('change', this.onModeChange.bind(this));
            if (Modes.isInMode('edit')) {
                this.onEditStart();
            }
            */
        }


        /**
         * Send request to the backend to resend all data values.
         * Typically only affect slow-updating values.
         */
        requestDataUpdate() {
            // Request new data in case the value_topic is slow or not sent out automatically.
            let update_topic = Octopy.topics['datasource']['update'];
            Events.publish(update_topic);
        }


        /**
         * Draws/updates the graphical representation of the manager.
         */
        render() {
            if (this.widget) {
                return;
            }
            let html = `
                <div id="nodes-manager">
                    <div class="title">Nodes</div>
                    <button id="add-node-button">Add node</button>
                </div>`;
            this.widget = $(html);
            let add_node_button = this.widget.find('#add-node-button');
            add_node_button.on('click', this.onAddNodeClick.bind(this));
            EditPanel.addModule(this.widget);
        }


        /**
         * Callback for mode change events.
         *
         * @param {event} e Event object
         * @param {string} mode The mode that changed state
         * @param {bool} state The active state of the mode - true = active, false = inactive.
         */
        onModeChange(e, mode, state) {
            if (mode === 'edit' && state) {
                this.onEditStart();
            }
            else if(mode == 'edit' && !state) {
                this.onEditEnd();
            }
        }


        /**
         * Helper method for the onModeChange callback for when the "edit" mode is activated.
         */
        onEditStart() {
            $('body').addClass('edit');
            this._editing = true;
        }


        /**
         * Helper method for the onModeChange callback for when the "edit" mode is deactivated.
         */
        onEditEnd() {
            this._editing = false;
            $('body').removeClass('edit');
            for (let node_id in this.nodes) {
                let node = this.nodes[node_id];
                if (node.dirty()) {
                    this.saveNode(node_id);
                    node.dirty(false);
                }
            }
        }


        /**
         * Callback for structure messages.
         *
         * @param {object} msg Event message object
         */
        onStructureMessage(msg) {
            let pat = /^([^\/]+).*\/structure$/;
            let match = pat.exec(msg.topic);
            if (match) {
                console.debug("New structure message");
                let prefix = match[1];
                if (msg.payload) {
                    let node_conf;
	            try {
                        node_conf = JSON.parse(msg.payload);
	            }
	            catch(err) {
	                console.error("Could not parse SECoP structure message");
	                console.error(msg);
	                return;
	            }
                    node_conf['prefix'] = prefix;
                    if (this._editing && this.nodes[node_conf['equipment_id']]) {
                        // Node exists and we're in edit mode so ignore updates
                        // to prevent loss of user edits.
                        return;
                    }
                    this.addNode(node_conf);
                }
                else {
                    // Empty/Null message means delete node
                    this.removeNode(prefix);
                }
                // Request data update
                this.requestDataUpdate();
            }
        }


        /**
         * Event handler for clicking on the Add Node button.
         *
         * @param {event} e Event object
         */
        onAddNodeClick(e) {
            if (this.node_count() > 0) {
                console.error("Octopy only supports a single node for now");
                return;
            }
            let conf = {
                "title": "Add SECNode",
                "done button": "Add"
            };
            if (! this.node_editor) {
                this.node_editor = new NodeEditor(conf);
            }
            this.node_editor.open(conf);
        }


        /**
         * Adds a new node from a data structure
         * received from the backend.
         *
         * @param {object} conf Node structure
         */
        addNode(conf) {
            let secnode;
            conf['parent'] = this;
            conf['id'] = conf['equipment_id'];
            if (conf['prefix'] in this.nodes) {
                secnode =  this.nodes[conf['prefix']];
                secnode.update(conf);
            }
            else {
                secnode = new SECNode(conf);
                this.nodes[conf['prefix']] = secnode;
            }
            secnode.render(this.node_list);
            // Ask the modes module to re-emit new events for all modes
            // to make sure the new node is set to the correct mode
            Modes.refresh();
        }


        /**
         * Serializes a node and send it to the backend (Node Manager) for
         * persistent storage.
         *
         * @param {object} conf Node structure
         */
        saveNode(node_id) {
            console.debug("Saving node '" + node_id + "'");
            let node = this.nodes[node_id];
            let structure = node.serialize();
            let submit_topic = Octopy.topics['node manager']['set node'];
            console.debug(structure);
            Events.publish(submit_topic, structure);

            /*
            if (node.id != node_id) {
                // The node's id has changed! Remove node and re-add it with new id.
                // This is a crude method, but the id is not supoposed to be changed often
                // and removing/adding guarantees proper reload of everything in all Octopy modules
                this.removeNode(node_id);
            }
            */
        }


        /**
         * Removes a node locally
         *
         * @param {node_id} ID of the node to be removed
         */
        removeNode(node_id) {
            if (! (node_id in this.nodes)) {
                console.error("Can't delete node \"" + node_id + "\" since it isn't loaded");
                return;
            }
            console.debug("Deleting SECNode: " + node_id);
            this.nodes[node_id].destroy();
            delete this.nodes[node_id];
        }

    }


    return new NodeManager();
});

