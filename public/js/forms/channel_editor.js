'use strict';

define(['jquery', 'events', 'octopy', 'json2', 'forms/form'], function($, Events, Octopy, json, Form) {

    class ChannelEditor extends Form {
        open(conf) {
            super.open();
            if ('title' in conf) {
                this.widget.find('.head > .title').text(conf['title']);
            }
            else {
                this.widget.find('.head > .title').text('Edit channel');
            }
            if ('done button' in conf) {
                this.widget.find('.foot button#form-done').text(conf['done button']);
            }
            else {
                this.widget.find('.foot button#form-done').text('Save');
            }
            this.datasource_id = conf['datasource id'];
            this.widget.find('.body #datasource-id').val(this.datasource_id);
            // Clear topics
            //this.widget.find('.body #value-topic').val('');
            //this.widget.find('.body #set-topic').val('');
            //this.widget.find('.body #get-topic').val('');
        }


        done() {
            this.close();
            let submit_topic = Octopy.topics['datasource']['set channels'];
            let conf = [ {
                'datasource id': this.widget.find('#datasource-id').val(),
                //'name': this.widget.find('#name').val(),
                //'channel id': this.widget.find('#channel-id').val(),
                'description': this.widget.find('#description').val(),
                'value topic': this.widget.find('#value-topic').val(),
                'set topic': this.widget.find('#set-topic').val(),
                'get topic': this.widget.find('#get-topic').val(),
                'set reference': this.widget.find('#set-reference').val(),
                'get reference': this.widget.find('#get-reference').val()
            } ];
            let regex = this.widget.find('#regex').val();
            console.debug(regex);
            if (regex) {
                conf[0]['hw reply regex'] = regex;
            }
            console.debug(conf);
            Events.publish(submit_topic, conf);
        }


        render() {
            let created = super.render();
            if (created) {
                // Widget was just created - create and add the form to the dialog body
                var body = this.createForm();
                this.widget.children('.body').append(body);
            }
            this.widget.attr('id', 'channel-form');
            let name_input = this.widget.find('.body #name');
            let id_input = this.widget.find('.body #channel-id');
            name_input.on('keyup', function(e) {
                let name = name_input.val().toLowerCase();
                name = name.replaceAll(/[^\w\d]/g, '_');
                // TODO: Add check and fix for names starting with digit
                id_input.val(name);
            }.bind(this));
        }


        createForm() {
            let html = `
              <ul>
                <li>
                  <label for="datasource-id" title="Datasource Id">Datasource ID</label>
                  <input type="text" id="datasource-id" disabled="true"></input>
                </li>
<!--
                <li>
                  <label for="name" title="Channel name">Name</label>
                  <input type="text" id="name"></input>
                </li>
                <li>
                  <label for="channel-id" title="Channel ID">ID</label>
                  <input type="text" id="channel-id"></input>
                </li>
-->
                <li>
                  <label for="description" title="Description">Description</label>
                  <input type="text" id="description"></input>
                </li>
                <li>
                  <label for="set-reference" title="Hardware command for setting value">Set Reference</label>
                  <input title="Hardware command for setting value" type="text" id="set-reference"></input>
                </li>
                <li>
                  <label for="get-reference" title="Hardware command for reading value">Get Reference</label>
                  <input title="Hardware command for reading value" type="text" id="get-reference"></input>
                </li>
                <li>
                  <label for="value-topic" title="MQTT topic that values read from the source should be published to for the channel">Value Topic</label>
                  <input title="MQTT topic that values read from the source should be published to for the channel" type="text" id="value-topic"></input>
                </li>
                <li>
                  <label for="set-topic" title="MQTT topic that the data source will listen to for setting the channel">Set Topic</label>
                  <input title="MQTT topic that the data source will listen to for setting the channel" type="text" id="set-topic"></input>
                </li>
                <li>
                  <label for="get-topic" title="MQTT topic used to trigger a new value read from the data source for the channel">Get Topic</label>
                  <input title="MQTT topic used to trigger a new value read from the data source for the channel" type="text" id="get-topic"></input>
                </li>
                <li>
                  <label for="regexp" title="Optional regex to use to extract data from compund reply">Regex</label>
                  <input title="Optional regex to use to extract data from compund reply" type="text" id="regex"></input>
                </li>
              </ul>`;
            return $(html);
        }
    }


    return ChannelEditor;
});
