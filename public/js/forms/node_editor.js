'use strict';

define(['jquery', 'jquery-ui', 'events', 'octopy', 'json2', 'forms/form'], function($, _, Events, Octopy, json, Form) {

    class NodeEditor extends Form {
        open(conf) {
            super.open();
            if ('title' in conf) {
                this.widget.find('.head > .title').text(conf['title']);
                this.widget.find('.body > .form-description').text(conf['title']);
            }
            else {
                this.widget.find('.head > .title').text('Edit channel');
                this.widget.find('.body > .form-description').text('Edit channel');
            }
            if ('done button' in conf) {
                this.widget.find('.foot button#form-done').text(conf['done button']);
            }
            else {
                this.widget.find('.foot button#form-done').text('Save');
            }
            let prefix = Octopy.prefix
            this.widget.find('.body #prefix').val(prefix);
            this.widget.find('.body #equipment_id').val(prefix);
            this.widget.find('.body #name').val(prefix);
            this.widget.find('.body #implementor').val('ESS');
        }


        done() {
            this.close();
            let submit_topic = Octopy.topics['node manager']['set node'];
            let conf = {
                'prefix': this.widget.find('#prefix').val(),
                'equipment_id': this.widget.find('#equipment_id').val(),
                'description': this.widget.find('#name').val() + "\n\n" + this.widget.find('#description').val(),
                'implementor': this.widget.find('#implementor').val(),
                'timeout': this.widget.find('#timeout').val(),
                "modules": {}
            }
            console.debug("Publishing new node '" + conf['equipment_id'] + "' configurationto Node Manager: " + submit_topic)
            Events.publish(submit_topic, conf);
        }


        render() {
            let created = super.render();
            if (created) {
                // Widget was just created - create and add the form to the dialog body
                var body = this.createForm();
                this.widget.children('.body').append(body);
            }
            this.widget.attr('id', 'channel-form');
            let name_input = this.widget.find('.body #name');
            let id_input = this.widget.find('.body #channel-id');
            name_input.on('keyup', function(e) {
                let name = name_input.val().toLowerCase();
                name = name.replaceAll(/[^\w\d]/g, '_');
                // TODO: Add check and fix for names starting with digit
                id_input.val(name);
            }.bind(this));
        }


        createForm() {
            let html = `
              <div class="form-description">Add SECNode</div>
              <ul>
                <li>
                  <label for="prefix" title="Octopy prefix">Prefix</label>
                  <input type="text" id="prefix" "prefix="prefix" disabled="true"></input>
                </li>
                <li>
                  <label for="equipment_id" title="Equipment ID">Equipment ID</label>
                  <input type="text" id="equipment_id" "equipment_id="equipment_id" disabled="true"></input>
                </li>
                <li>
                  <label for="name" title="Name">Name</label>
                  <input type="text" id="name" "name="name"></input>
                </li>
                <li>
                  <label for="description" title="description">Description</label>
                  <input type="text" id="description" "description="description"></input>
                </li>
                <li>
                  <label for="implementor" title="Implementor">Implementor</label>
                  <input type="text" id="implementor" "implementor="implementor"></input>
                </li>
                <li>
                  <label for="timeout" title="Timeout">Timeout</label>
                  <input type="number" id="timeout" "timeout="timeout"></input>
                </li>
              </ul>
            `;
            return $(html);
        }
    }


    return NodeEditor;

});
