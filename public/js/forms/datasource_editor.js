'use strict';

define(['jquery', 'jquery-ui', 'events', 'octopy', 'json2', 'forms/form'], function($, _, Events, Octopy, json, Form) {

    class DataSourceEditor extends Form {
        open(conf) {
            super.open();
            if ('title' in conf) {
                this.widget.find('.head > .title').text(conf['title']);
            }
            else {
                this.widget.find('.head > .title').text('Edit Data Source');
            }
            if ('done button' in conf) {
                this.widget.find('.foot button#form-done').text(conf['done button']);
            }
            else {
                this.widget.find('.foot button#form-done').text('Save');
            }
        }


        done() {
            this.close();
            let selected_tab = this.widget.find('[role="tab"][aria-selected="true"]').attr('id');
            let submit_topic;
            let conf;
            switch(selected_tab) {
            case 'tab-serial':
                conf = {
                    'device': this.widget.find('#serial-source #device').val(),
                    'baudrate': this.widget.find('#serial-source #baudrate').val(),
                    'databits': this.widget.find('#serial-source #databits').val(),
                    'stopbits': this.widget.find('#serial-source #stopbits').val(),
                    'parity': this.widget.find('#serial-source #parity').val(),
                    'delimiter': this.widget.find('#serial-source #delimiter').val(),
                    'pollinterval': this.widget.find('#serial-source #pollinterval').val()
                }
                submit_topic = Octopy.topics['datasource']['config serial'];
                break;
            case 'tab-socket':
                conf = {
                    'host': this.widget.find('#socket-source #host').val()
                }
                let port = this.widget.find('#socket-source #port').val();
                if (port) {
                    port = parseInt(port);
                    conf['port'] = port;
                }
                submit_topic = Octopy.topics['datasource']['config socket'];
                break;
            case 'tab-mqtt':
                conf = {
                    'broker': this.widget.find('#mqtt-source #broker').val(),
                    'prefix': this.widget.find('#mqtt-source #prefix').val()
                }
                submit_topic = Octopy.topics['datasource']['config mqtt'];
                break;
            default:
                throw("No data source tab selected");
            }
            Events.publish(submit_topic, conf);
        }


        render() {
            let created = super.render();
            if (created) {
                // Widget was just created - create and add the form to the dialog body
                let body = this.createForm();
                this.widget.children('.body').append(body);
                this.widget.children('.body').tabs();
            }
            this.widget.attr('id', 'datasource-form');
        }


        createForm() {
            let html = `
              <ul>
                <li id="tab-mqtt"><a href="#mqtt-source">MQTT</a></li>
                <li id="tab-socket"><a href="#socket-source">Ethernet</a></li>
                <li id="tab-serial"><a href="#serial-source">Serial</a></li>
              </ul>
              <div id="mqtt-source">
                <div class="form-description">MQTT Data Source communicating directly via MQTT</div>
                <ul>
                  <li>
                    <label for="prefix" title="Broker">Broker</label>
                    <input type="text" id="broker" "name="broker" value="localhost" disabled></input>
                  </li>
                  <li>
                    <label for="prefix" title="Topic Prefix">Topic Prefix</label>
                    <input type="text" id="prefix" "name="prefix"></input>
                  </li>
                </ul>
              </div>
              <div id="socket-source">
                <div class="form-description">Ethernet socket datasource using a request-response protocol</div>
                <ul>
                  <li>
                    <label for="host" title="Host">Hostname/IP-address</label>
                    <input type="text" id="host" "name="host"></input>
                  </li>
                  <li>
                    <label for="port" title="Port">TCP/IP port</label>
                    <input type="text" id="port" "name="port"></input>
                  </li>
                </ul>
              </div>
              <div id="serial-source">
                <div class="form-description">Serial datasource communicating over RS-232)</div>
                <ul>
                  <li>
                    <label for="device" title="Serial-Device">Serial Device</label>
                    <input type="text" id="device" "name="device"></input>
                  </li>
                  <li>
                    <label for="baudrate" title="Baud Rate">Baud rate</label>
                    <input type="text" id="baudrate" "name="baudrate"></input>
                  </li>
                  <li>
                    <label for="databits" title="Data Bits">Number of data bits</label>
                    <input type="text" id="databits" "name="databits"></input>
                  </li>
                  <li>
                    <label for="stopbits" title="Stop Bits">Number of stop bits</label>
                    <input type="text" id="stopbits" "name="stopbits"></input>
                  </li>
                  <li>
                    <label for="parity" title="Parity">Parity</label>
                    <input type="text" id="parity" "name="parity"></input>
                  </li>
                  <li>
                    <label for="delimiter" title="Delimiter">Command delimiter</label>
                    <input type="text" id="delimiter" "name="delimiter"></input>
                  </li>
                  <li>
                    <label for="pollinterval" title="Poll Interval">Poll interval</label>
                    <input type="text" id="pollinterval" "name="pollinterval"></input>
                  </li>
                </ul>
              </div>
            `;
            return $(html);
        }
    }


    return DataSourceEditor;

});
