'use strict';

define(['jquery', 'events', 'json2'], function($, Events, json) {

    class Form {
        constructor() {
            this.render();
            this.widget.find('.foot button#form-cancel').on('click', this.cancel.bind(this));
            this.widget.find('.foot button#form-done').on('click', this.done.bind(this));
        }


        open() {
            $('body').find('#form-background').fadeIn();
            this.widget.fadeIn();
            $(document).on('keydown', function(e) {
                if (e.key === "Escape") {
                    this.cancel();
                    e.stopPropagation();
                }
                else if (e.key === "Enter") {
                    this.done();
                    e.stopPropagation();
                }
            }.bind(this));
        }


        close() {
            $(document).off('keydown');
            $('body').find('#form-background').fadeOut();
            this.widget.fadeOut();
        }


        cancel() {
            this.close();
        }


        done() {
            console.error("Form has no method for handling data");
            this.close();
        }


        render() {
            var created = false;
            var bg = $('body').find('#form-background');
            if (bg.length < 1) {
                // Shared form background doesn't exist - create it.
                bg = $('<div id="form-background">');
                $('body').append(bg);
            }
            if (!this.widget) {
                this.widget = this.createWidget();
                $('body').append(this.widget);
                created = true;
            }
            this.widget.hide();
            return created;
        }


        createWidget() {
            var html = `
                <div class="form panel">
                    <div class="head">
                        <div class="title"></div>
                    </div>
                    <div class="body"></div>
                    <div class="foot">
                      <button id="form-done">Done</button>
                      <button id="form-cancel">Cancel</button>
                    </div>
                </div>
            `;
            var widget = $(html);
            return widget;
        }

    }


    return Form;
});
