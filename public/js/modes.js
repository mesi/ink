/**
 * @file modes.js
 * @author Andreas Hagelberg
 *
 * Implements the Modes module
 */


'use strict';

define(['jquery', 'events'], function($, Events) {

    /**
     * @class Modes
     *
     * The Modes module keeps track of global modes for the entire GUI.
     * The modes can be arbitrarily defined by other modules, but a few are
     * predefined.
     */
    class Modes {
        constructor() {
            this.modes = {
                'edit': false,
                'user': true,
                'advanced': false,
                'expert': false
            };
        }

        /**
         * Called from main system to start the module. The Modes module has
         * nothing that needs to be done at starta so this method is empty.
         */
        start() {
        }


        /**
         * Re-emits the state of all known modes
         */
        refresh() {
            $.each(this.modes, function(mode, state) {
                this.signalModeChange(mode);
            }.bind(this));
        }


        /**
         * Returns the state (true/false) of all known modes
         *
         * @return {hash} All modes as keys and their state as values
         */
        getModes() {
            let modes = {}
            Object.assign(modes, this.modes);  // Make copy of modes data structure
            return modes;
        }


        /**
         * Toggles the state of a mode and broadcasts the new state
         *
         * @param {string} mode Name of the mode to toggle
         */
        toggleMode(mode) {
            if (! mode in this.modes) {
                console.error("Can't toggle non-existent mode: " + mode);
                return;
            }
            this.modes[mode] = !this.modes[mode];
            this.signalModeChange(mode);
        }


        /**
         * Sets the state of a mode and broadcasts the new state
         *
         * @param {string} mode Name of the mode to toggle
         * @param {bool} state The desired state to se the mode to:
         *                     true = active, false = inactive
         */
        setMode(mode, state) {
            if (! mode in this.modes) {
                console.error("Can't set non-existent mode: " + mode);
                return;
            }
            this.modes[mode] = !!state;
            this.signalModeChange(mode);
        }


        /**
         * Returns the current state of a mode
         *
         * @param {string} mode Name of the mode to return the state of
         *
         * @return {bool} The state of the mode: true = active, false = inactive
         */
        isInMode(mode) {
            if (mode in this.modes) {
                return !!(this.modes[mode]);
            }
            else {
                return false;
            }
        }


        /**
         * Helper function that broadcasts the current state of a mode.
         *
         * @param {string} mode Name of the mode to broadcast
         */
        signalModeChange(mode) {
            $(this).trigger('change', [mode, this.modes[mode]]);
        }
    }


    return new Modes();
});
