/**
 * @file octopy.js
 * @author Andreas Hagelberg
 *
 * Implements the Octopy server module
 */

'use strict';

define(['jquery', 'events', 'json2'], function($, Events, json) {

    /**
     * @class Octopy
     *
     * This module is a representation of the backend. It's main purpose is to
     * receive global server configuration parameters and make them available
     * to the rest of the frontend, and to implement common functions for
     * interacting with the backend on a global level.
     * It's important that this module is started before the event module since
     * the server config message is sent as soon as a connection is established.
     */
    class Octopy {
        constructor() {
            this.config_topic = 'octopy-config';
            this.server_config = {}
            this._ready = false;

            this.topics = {
                'datasource': {
                    "config serial": "$PREFIX/octopy/datasource/serial/config",
                    "config socket": "$PREFIX/octopy/datasource/socket/config",
                    "config mqtt": "$PREFIX/octopy/datasource/mqtt/config",
                    "remove source": "$PREFIX/octopy/datasource/sources/remove",
                    "sources": "$PREFIX/octopy/datasource/sources",
                    "channels": "$PREFIX/octopy/channels",
                    "set channels": "$PREFIX/octopy/datasource/channels/set",
                    "remove channels": "$PREFIX/octopy/datasource/channels/remove",
                    'update': '$PREFIX/octopy/datasource/channels/update',
                    "status": "$PREFIX/octopy/datasource/hwgw/status",
                    "links": "$PREFIX/octopy/datasource/hwgw/links"
                },
                'ink': {
                    "reload client": "$PREFIX/octopy/ink/clients/update"
                },
                'logging': {
                    'prefix': '$PREFIX/octopy/log/'
                },
                'node manager': {
                    'structure': '$PREFIX/octopy/structure',
	            'set node': '$PREFIX/octopy/nodemanager/node/set'
                }
            };
        }


        /**
         * Starts the module - subscribes to server config messages.
         */
        start() {
            Events.subscribe(this.config_topic, this.onServerConfigMessage.bind(this));
        }


        /**
         * Event handler for server config messages
         *
         * @param {object} msg Event message object
         */
        onServerConfigMessage(msg) {
            this._ready = true;
            if (! msg.payload) {
                console.error("Server config message received on topic \"" + msg.topic + "\" was empty");
                return false;
            }
            try {
                var config = JSON.parse(msg.payload);
                this.server_config = config;
                console.info("Received server config");
                this.prefix = this.server_config['mqtt']['prefix'];
                this.processTopics(this.topics, this.prefix);
            }
            catch(e) {
                console.error("Error parsing server config message received on topic: " + msg.topic);
                console.error(e);
                console.error(msg.payload);
                return false;
            }
            this.updateBackground();
            $(this).trigger('ready');
        }


        updateBackground() {
            let version_string = this.getServerConfigValue('version');
            let version_box = $('<div>').
                text('version ' + version_string).
                addClass('octopy-version');
            $('body').append(version_box);
        }


        processTopics(topics, prefix) {
            for (const [key, value] of Object.entries(topics)) {
                if (value instanceof Object) {
                    this.processTopics(value, prefix);
                }
                else if (typeof value === 'string') {
                    topics[key] = value.replace(/\$PREFIX/, prefix);
                }
            }
            return topics;
        }


        /**
         * Returns the ready status of the Octopy module - mainly
         * for the loading of the server config.
         *
         * @return {bool} True if the system is ready, false otherwise.
         */
        ready() {
            return this._ready;
        }


        /**
         * Returns a server config value
         *
         * @param {string} path Key for the value. The server config is structured
         *                      as a tree and the key is formatted as a file path
         *                      to access a particular key.
         */
        getServerConfigValue(path) {
            let keys = path.split('/');
            let node = this.server_config;
            for (const key of keys) {
                if (key in node) {
                    node = node[key];
                }
                else {
                    console.error("Missing server config value: " + path);
                }
            }
            return node;
        }


        /**
         * Makes a global request to all datasources to update all their channels
         * and broadcast current values on all value-topics.
         */
        requestDataUpdate() {
            let update_topic = this.topics['datasource']['update'];
            Events.publish(update_topic);
        }

    }

    return new Octopy();
});

