/**
 * @context-menu.js
 * @author Andreas Hagelberg
 *
 * Implements a JQuery-UI widget for a simple menu
 */

"use strict";

define(['jquery', 'jquery-ui'], function($, jqueryui) {
    var widget = {
        options: {
            _visible: false
        },


        _create: function() {
            this.element.addClass('context-menu handle');
            let icon = $('<div class="icon"></div><div class="icon"></div><div class="icon"></div>');
            this.element.append(icon);
            this._menu_el = $('<div class="context-menu menu"></div>');
            this._menu_el.hide();
            this.element.append(this._menu_el);
            this.element.on('click', this.toggleMenu.bind(this));
            this._items = {};
        },


        _init: function() {
            this._value = false;
            this._super();
            this.option(this.options);
        },


        showMenu: function() {
            this.hideAllMenus();
            this._menu_el.show();
            this._visible = true;
            $(window).on('click', function() {
                this.hideAllMenus();
                $(window).off('click');
            }.bind(this));
        },


        hideMenu: function() {
            if (this._menu_el) {
                this._menu_el.hide();
            }
            this._visible = false;
        },


        hideAllMenus: function() {
            $.each($('.context-menu.handle'), function(i, menu) {
                if ($(menu).context_menu('instance') != undefined) {
                    $(menu).context_menu('hideMenu');
                }
            });
        },


        toggleMenu: function(e) {
            e.stopPropagation();
            if (this._visible) {
                this.hideMenu();
            }
            else {
                this.showMenu();
            }
        },


        setItems: function(items) {
            $.each(items, function(key, conf) {
                if (! (key in this._items)) {
                    let item = $('<div class="menu-item">' + conf['title'] + '</div>');
                    item.attr('id', key);
                    item.on('click', function(e) {
                        this.element.trigger('select', key);
                    }.bind(this));
                    this._menu_el.append(item);
                    this._items[key] = item;
                }
                let item = this._items[key];
                if ('title' in conf) {
                    item.html(conf['title']);
                }
                if (conf['disabled']) {
                    item.addClass('disabled');
                }
                else {
                    item.removeClass('disabled');
                }
                if (conf['hidden']) {
                    item.hide();
                }
                else {
                    item.show();
                }
            }.bind(this));
        },


        _setOption: function(option, value) {
            this._super(option, value);
            switch(option) {
            case 'items':
                this.setItems(value);
                break;
            }
        }
    };


    return $.widget('ink.context_menu', widget);
});
