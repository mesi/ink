/**
 * @file main.js
 * @author Andreas Hagelberg
 *
 * @description Main entry point. Sets up options and paths for Require.js
 *              and starts all modules in order.
 */

'use strict';

require.config({
    baseUrl: '/js',
    paths: {
        'jquery': 'lib/jquery',
        'jquery-ui': 'lib/jquery-ui',
        'json2': 'lib/json2',
        'loadcss': 'lib/loadcss',
        'chart': 'lib/chart'
    },
    waitSeconds: 20
});

requirejs(
    [
        'jquery',
        'events',
        'octopy',
        'modes',
        'edit_panel',
        'datasource/datasource_manager',
        'node_manager',
        'watchdog',
        'widgets/widgets_palette',
        'logwindow',
        'sequencer/sequence_editor'
    ],
    function(
        $,
        Events,
        Octopy,
        Modes,
        EditPanel,
        DataSourceManager,
        NodeManager,
        Watchdog,
        WidgetsPalette,
        LogWindow,
        SequenceEditor
    ) {
        $(document).ready(() => {
            Octopy.start();
            Modes.start();
            Events.start();
            EditPanel.start();
            NodeManager.start();
            WidgetsPalette.start();
            DataSourceManager.start();
            Watchdog.start();
            LogWindow.start();
            SequenceEditor.start();
        });

        $(window).bind('beforeunload', () => {
            Events.stop();
        });
    }
);
