/**
 * @file watchdog.js
 * @author Andreas Hagelberg
 *
 * Implements the the Watchddog module
 */



'use strict';

/* JQuery Apparatus widget */
define(['jquery', 'events', 'octopy', 'modes'], function($, Events, Octopy, Modes) {

    /**
     * @class Watchdog
     *
     * The watchdog module reloads the entire GUI every 8 hours as a crude fix for
     * potential memory leaks and other problems. It also listens for reload
     * signals over MQTT so that the GUI can be reloaded remotely.
     * In order to prevent data loss any reload happening while the system is
     * in its global edit mode will be delayed.
     */
    class Watchdog {
        /**
         * @constructor
         */
        constructor() {
            this.reload_interval =  8*3600*1000;  // 8 hours in ms
            this.delayed_reload_time = 60*1000; // 10 minutes in ms
        }


        /**
         * Starts the watchdog module.
         */
        start() {
            this.set_reload_timeout();
            this.setup_event_monitor();
        }


        /**
         * Setups the reload timeout timer.
         */
        set_reload_timeout(timeout) {
            if (! timeout) {
                timeout = this.reload_interval;
            }
            this._reload_timer = setTimeout(this.reload.bind(this), timeout);
        }


        /**
         * Setups the reload event monitor. Since it gets the reload topic from
         * the server config it waits for the server config to be loaded first.
         * It does this by setting up a timeout that calls this function again
         * and again every 2 seconds until the server module is ready.
         */
        setup_event_monitor() {
            if (Octopy.ready()) {
                try {
                    let reload_topic = Octopy.topics['ink']['reload client'];
                    Events.subscribe(reload_topic, function(msg) { this.reload(); }.bind(this));
                    console.debug('Reload event monitor added');
                }
                catch(err) {
                    console.debug(err);
                }
            }
            else {
                console.debug("Watchdog is waiting for server config to load");
                this._setup_wait_timer = setTimeout(this.setup_event_monitor.bind(this), 2000);
            }
        }


        /**
         * Reloads the page, unless the GUI is in edit mode in which case it
         * resets the reload timer with a shorter timeout.
         */
        reload() {
            if (! Modes.isInMode('edit')) {
                location.reload();
            }
            else {
                console.info("Watchdog reload was surpressed because system is in edit mode - delaying reload");
                this.set_reload_timeout(this.delayed_reload_time);
            }
        }
    }


    return new Watchdog();
});
