/**
 * @file events.js
 * @author Andreas Hagelberg
 *
 * Implements the event handling system
*/

'use strict';

define(['jquery', 'json2'], function($) {

    /**
     * @class Events
     *
     * This is the core for all communication betwen the backend and frontend.
     * The Events module connects to the backend via a websocket and starts
     * receiving messages from it. Other modules in the front end can then
     * subscribe to certain topics and also transmit their own messages back to
     * the backend via this module. Together with the web server it extends the
     * MQTT functionality used for communication in the backend to the frontend.
     */
    class Events {
        /**
         * @constructor
         */
        constructor() {
            this.port = 3001;
            this.protocol = 'event.v4';
            this.connected = false;

            this._subscribers = [];
            this._running = false;
            this._connection_attempts = 0;
        }


        /**
         * Internal method that takes a message received from the backend
         * over the websocket and distributes it to all modules that have
         * registered themselves as subscribers to it.
         *
         * @private
         * @param {object} event A hash consisting of "topic" and "payload".
         */
        _relayEvent(event) {
            //      console.debug('New event: ' + event.topic);
            let finished_calls = [];
            try {
                $.each(this._subscribers, (i, sub) => {
                    if (! sub) {
                        // Safe-guard against deletions from _subscribers list
                        // during even handling.
                        return;
                    }
                    let topic = sub[0];
                    let subscriber = sub[1];
                    if (topic.test(event.topic)) {
                        // Topic Match!
                        if (typeof(subscriber) == 'function')
                            subscriber(event);
                        else if (typeof(subscriber.onMessage) == 'function')
                            subscriber.onMessage(event);
                        else
                            throw('Invalid subscriber');
                    }
                });
            } catch(err) {
                console.error(err);
                console.error(err.stack);
                console.error(event);
            }
        }


        /**
         * Any module that wants to listen to messages coming from the
         * backend needs to call this method to be added to the list of
         * subscribers for each topic of interest. Wild-cards are allowed
         * in the topic according to the MQTT standard.
         *
         * @param {string} topic The topic(s) to subscribe to
         * @param {object} subscriber The module or callback function that
         *                            will be called when a new message matching
         *                            the topic is received from the backend.
         *                            If it's an object it must have a method
         *                            called "onMessage".
         */
        subscribe(topic, subscriber) {
            if (!topic) {
                throw('No topics specified to subscribe to');
            }
            let pat = topic;
            pat = pat.replace('/', '\/');
            pat = pat.replace('+', '[^\\/]+');
            pat = pat.replace('#', '.+');
            pat = '^' + pat + '$';
            pat = pat = new RegExp(pat);
            for (let i = 0; i < this._subscribers.length; i++) {
                let sub = this._subscribers[i];
                if (sub[1] === subscriber && sub[2] === topic) {
                    // Subscription already exists
                    return;
                }
            }
            this._subscribers.push([pat, subscriber, topic]);
            console.debug('Subscription added for topic: ' + topic);
        }


        /**
         * Removes a subscription.
         *
         * @param {string} topic The topic/pattern to unsubscribe from. Must
         *                       match exactly the string used when subscribed.
         * @param {object} subscriber The module or callback function that wants
         *                            to unsubscribe. Must be the exact same
         *                            function or object that originally
         *                            subscribed.
         */
        unsubscribe(topic, subscriber) {
            if (!subscriber) {
                throw('Missing or invalid argument for event.unsubscribe()');
            }
            for (let i = 0; i < this._subscribers.length; i++) {
                let sub = this._subscribers[i];
                if (sub[1] == subscriber && sub[2] == topic) {
                    this._subscribers.splice(i, 1);
                    break;
                }
            }
            console.debug('Subscription removed for topic: ' + topic);
        }


        /**
         * Publish a message over MQTT to the rest of the system via the
         * websocket.
         *
         * @param {string} topic The topic to send the message on
         * @param {object} payload The actual data to send. This is assumed
         *                         to be an object of some kind that can be
         *                         converted to JSON.
         */
        publish(topic, payload) {
            /* Construct the event data object */
            let event = {
                topic: topic,
                payload: payload || null
            };
            if (this._socket) {
                /* Send over WebSocket */
                let data = JSON.stringify(event);
                this._socket.send(data);
            }
        }


        /**
         * Internal method that opens up the websocket to the webserver in
         * the backend. It also sets up callback functions for handling
         * various events for the websocket including new messages and
         * errors. If the connection is broken for some reason the method
         * will try to reconnect.
         *
         * @private
         */
        _openSocket() {
            if (this._temp_socket)
                return;
            if (this._reconnect_timer) {
                clearTimeout(this._reconnect_timer);
                delete this._reconnect_timer;
            }
            let sock;
            let url;
            if (this.host) {
                url = this.host;
                if (!url.match(/https?\:\/\//)) {
                    url = 'http://' + url;
                }
            }
            else {
                url = window.location + '';
            }
            if (this.port) {
                url = url.replace(/:\d+/, ':' + this.port);
            }
            let secure = (/^https/).test(url);
            url = url.match(/:\/\/[^\/]+/);
            url = (secure ? 'wss' : 'ws') + url;
            console.debug('Connecting to WebSocket at ' + url);
            if (window.WebSocket)
                sock = new WebSocket(url, this.protocol);
            else if (window.MozWebSocket)
                sock = new MozWebSocket(url, this.protocol); // Older Firefox
            if (!sock) {
                console.error('No WebSocket support found');
                return;
            }
            this._temp_socket = sock;
            // open callback
            sock.onopen = function() {
                console.info('Websocket connection established to ' + url + '.');
                this._socket = this._temp_socket;
                delete this._temp_socket;
                this.connected = true;
                this._connection_attemps = 0;
            }.bind(this);
            // message callback
            sock.onmessage = function(msg) {
	        let boundary = msg.data.indexOf("\n");
	        let topic = msg.data.substr(0, boundary);
	        let payload = msg.data.substr(boundary+1);
	        let event = { 'topic': topic, 'payload': payload };
                this._relayEvent(event);
            }.bind(this);
            // error callback
            sock.onerror = function(e) {
                console.error(e);
            }.bind(this);
            // close callback
            sock.onclose = function() {
                this.connected = false;
                console.info('Websocket connection closed.');
                if (this._socket)
                    delete this._socket;
                if (this._temp_socket)
                    delete this._temp_socket;
                // Try to reopen the socket
                let reconnect_delay = this._connection_attempts * 2;
                this._connection_attempts++;
                if (reconnect_delay > 10) {
                    reconnect_delay = 10;
                }
                console.info('Trying to reconnect to websocket in ' + reconnect_delay.toString() + ' seconds');
                this._reconnect_timer = setTimeout(this._openSocket.bind(this), reconnect_delay * 1000);
            }.bind(this);
            /* Save the socket temporarily until it's actually opened */
            this._temp_socket = sock;
        }


        /**
         * Internal method for closing the websocket and ending the
         * communication with the backend.
         *
         * @private
         */
        _closeSocket() {
            if (this._temp_socket) {
                this._temp_socket.onclose = function() {};  // Disable onclose event handler
                this._temp_socket.close();
                this._temp_socket = null;
            }
            if (this._socket) {
                this._socket.onclose = function() {};  // Disable onclose event handler
                this._socket.close();
                this._socket = null;
            }
        }


        /**
         * Starts the communication with the backend.
         */
        start() {
            if (!this._running) {
                console.log('Starting event system');
                this._running = true;
                this._openSocket();
            }
        }


        /**
         * Stops the communication with the backend.
         */
        stop() {
            if (this._running) {
                console.log('Stopping event system');
                this._closeSocket();
                this._subscribers = {};
                this._callers = {};
                this._running = false;
            }
        }

    }


    return new Events();
});
