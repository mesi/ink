/**
 * @file logwindow.js
 * @author Andreas Hagelberg
 *
 * Implements a log window
 */


'use strict';

define(['jquery', 'events', 'octopy'], function($, Events, Octopy) {

    /**
     * @class LogWindow
     *
     * The log window listens to log messages sent from the backend and displays
     * them in a easy-to-read format.
     */
    class LogWindow {
        constructor() {
            this.max_messages = 1000;
            this._enabled = false;
        }


        /**
         * Starts the module. Creates the widget and add a button to the toolbar.
         */
        start() {
            console.debug("Starting log window");
            this.render();
        }


        /**
         * Enables the log window. Shows the window and starts listening to log messages.
         */
        enable() {
            this._enabled = true;
            this.widget.show();
            let log_topics = Octopy.topics['logging']['prefix'] + '/#';
            Events.subscribe(log_topics, this);
            // Patch the browser console warning function
            this._console_warn = console.warn;
            console.warn = function() {
                this.addMessage('warning', {
                    'source': 'Ink',
                    'timestamp': Date.now() / 1000,
                    'message': arguments[0]
                });
                return this._console_warn.apply(console, arguments);
            }.bind(this);
            // Patch the browser console error function
            this._console_error = console.error;
            console.error = function() {
                this.addMessage('error', {
                    'source': 'Ink',
                    'timestamp': Date.now() / 1000,
                    'message': arguments[0]
                });
                return this._console_error.apply(console, arguments);
            }.bind(this);
        }


        /**
         * Disables the log window. Stops listening to log messages and hides the window.
         */
        disable() {
            this._enabled = false;
            this.widget.hide();
            let log_topics = Octopy.topics['logging']['prefix'] + '/#';
            Events.unsubscribe(log_topics, this);
            // Restore the browser console functions
            console.warn = this._console_warn;
            console.error = this._console_error;
        }


        /**
         * Callback function for new messages called from the Events module.
         *
         * @param {string} msg An event message in JSON format.
         */
        onMessage(msg) {
            let payload = JSON.parse(msg.payload);
            let level = msg.topic.match(/[^\/]+$/) + '';
            this.addMessage(level, payload);
        }


        /**
         * Add a new log message to the window.
         *
         * @param {string} level The log level of the message, one of error, warning, info and debug.
         * @param {object} msg The log message.
         * @param {number} msg['timestamp'] A Unix timestamp for when the message was generated.
         * @param {string} msg['source'] The program/module that generated the message.
         * @param {message} msg['message'] The log message.
         */
        addMessage(level, msg) {
            let timestamp = new Date(msg['timestamp'] * 1000).toISOString();
            let source = msg['source'];
            let file_info = '';
            if (msg['stack']) {
                let last_line = msg['stack'][msg['stack'].length-2]; // Hack - assumes the last call was to a common log-function
                file_info = last_line['filename'] + ':' + last_line['lineno'];
            }
            let html = '<div class="log-message ' + level.toLowerCase() + '">' +
                '<span class="timestamp">' + timestamp + '</span>' +
                '<span class="level">' + level.toUpperCase() + '</span>' +
                '<span class="source">' + source.toUpperCase() + '</span>' +
                '<span class="file">' + file_info + '</span>' +
                '<span class="message">' + msg['message'] + '</span>' +
                '</div>';
            let msg_el = $(html);
            this._messages_el.prepend(msg_el);
            let messages = this._messages_el.find('.log-message');
            if (messages.length > this.max_messages) {
                messages[this.max_messages].remove();
            }
        }


        /**
         * Creates the DOM elements for the log window and the toolbar button.
         *
         */
        render() {
            if (this.widget) {
                return;
            }
            this.widget = $('body').find('#log-window');
            this.widget.addClass('panel');
            this.widget.append($('<div class="head"><div class="name">System Log</div></div>'));
            this._messages_el = $('<div class="messages"></div>');
            this.widget.append(this._messages_el);
            this.widget.hide();
            // Add show button to toolbar
            let toolbar = $('#main-toolbar');
            let btnEdit = $('<button id="show-log-button">SYSTEM LOG</button>');
            btnEdit.on('click', function(e) {
                if (this._enabled) {
                    this.disable();
                }
                else {
                    this.enable();
                }
            }.bind(this));
            toolbar.append(btnEdit);
        }
    }

    return new LogWindow();
});
