const WebSocketServer = require('ws').Server;
const GatewayClient = require('./gateway_client');

class MqttWSGateway {
    constructor(mqttHost, mqttPort, mqttUsername, mqttPassword, wsPort, server_config) {
        this.mqttClient = null;
        this.mqttHost = mqttHost;
        this.mqttPort = mqttPort;
        this.mqttUsername = mqttUsername;
        this.mqttPassword = mqttPassword;
        this.server_config = server_config;
        this.wsPort = wsPort || 1888;
        this.wss = null;
        this.clients = {};
    }

    start() {
        if (this._wss) {
            return;
        }
        // Start websocket server
        this.wss = new WebSocketServer({port: this.wsPort});
        this.wss.on('connection', (ws) => {
            console.log("Ink:DEBUG:New WebSocket client connected");
            let client = new GatewayClient(ws, this.mqttHost, this.mqttPort, this.mqttUsername, this.mqttPassword, this.server_config);
            this.clients[client.id] = client;
            ws.on('close', () => {
                console.log("Ink:DEBUG:Client " + client.id + " disconnected");
                delete this.clients[client.id];
            });
            ws.on('error', (err) => {
                console.log('Ink:ERROR:WebSocket client error:');
                console.log(err);
                client.stop();
                delete this.clients[client.id];
            });
            client.connect();
        });
        console.log("Ink:INFO:Started WebSocket server on port " + this.wsPort);
    }
}

module.exports = MqttWSGateway;
