const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const MqttWSGateway = require('./mqtt_ws_gateway');
const config_loader = require('./config_loader');

// Load configuration
const config = config_loader('./config/config/');
// Ink uses the Express framework to simplify the creation of a web app
let app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Curate server config to send to the client.
// Include needed/useful settings, but leave out security sensitive settings.
let server_config = {
    'system id': config['system id'],
    'version': config['version'],
    'mqtt': {
        'prefix': config['mqtt']['prefix']
    },
    'ink': config['ink']
}

// Setup and start the MQTT-to-Websocket gateway
let gateway = new MqttWSGateway(config['mqtt']['host'],
                                config['mqtt']['port'],
                                config['mqtt']['username'],
                                config['mqtt']['password'],
                                config['ink']['websocket port'],
                                server_config
                               );
gateway.start();

// Setup route for handling MQTT messages sent over HTTP.
// This is generally only used for debugging - all real traffic goes
// through the WebSocket interface of the gateway.
app.post("/send-mqtt", function(req, res) {
    let topic = req.body.message.topic;
    let payload = req.body.message.payload;
    gateway.publish(topic, payload);
    res.status(200).send("Message sent to mqtt");
});

// Setup route to serve the static files (HTML, CSS, JS, images, etc.)
app.use('/', express.static(path.join(__dirname, config['ink']['frontend folder'])));

// Start the web server
let server = app.listen(config['ink']['frontend port'], function () {
    console.log("Ink:INFO:Started Ink server on port ", server.address().port);
});
