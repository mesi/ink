# Ink
Ink is a self-configuring GUI for low-level administration and control of MESI's
sample environment systems that uses SECoP over MQTT.

## Install
* Install Node and NPM (Node Package Manager)
* Clone this repo
* Run "npm install" in the cloned directory to install required Node modules

## Start
To start Ink use "start" to execute the start shell script, or use "node ink.js"
to start it directly. Ink will then respond to HTTP requests on port 3000.
