const DEFAULT_CONFIG_PATH = 'config/';
const fs = require('fs');
const path = require('path');

function load_config(dir) {
    if (!dir) {
        dir = DEFAULT_CONFIG_PATH;
    }
    let config = {}
    let config_files = find_files(dir);
    filenames = Object.keys(config_files).sort();
    filenames.forEach(function(filename) {
	let file = config_files[filename];
        let rawdata = fs.readFileSync(file);
        let new_config = JSON.parse(rawdata);
        config = merge_configs(new_config, config);
    });
    if (! config['mqtt']['prefix']) {
        config['mqtt']['prefix'] = config['system id'];
    }
    post_process(config, config['mqtt']['prefix']);
    return config;
}


function find_files(dir) {
    let found_files = {};
    files = fs.readdirSync(dir);
    files.forEach(function(file) {
        file = path.resolve(dir, file);
        let stat = fs.statSync(file);
        if (stat && stat.isDirectory()) {
            let sub_files = find_files(file);
	    let sub_filenames = Object.keys(sub_files);
	    sub_filenames.forEach(function(sub_filename) {
		found_files[sub_filename] = sub_files[sub_filename];
	    });
        }
        else if (path.extname(file) == '.json') {
	    let filename = file.split('/').pop();
            found_files[filename] = file;
        }
    });
    return found_files;
}


function merge_configs(source, destination) {
    for (const [key, value] of Object.entries(source)) {
        if (value instanceof Object && key in destination) {
            destination[key] = merge_configs(value, destination[key]);
        }
        else {
            destination[key] = value;
        }
    }
    return destination;
}

function post_process(conf, prefix) {
    for (const [key, value] of Object.entries(conf)) {
        if (value instanceof Object) {
            post_process(value, prefix);
        }
        else if (typeof value === 'string') {
            conf[key] = value.replace(/\$PREFIX/, prefix);
        }
    }
    return conf;
}


module.exports = load_config;
